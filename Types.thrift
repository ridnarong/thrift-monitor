namespace java com.nectec.est.zigbee_gateway.zgd.services.thrift.zgd.type

enum RPCProtocol {
    GRIP = 1,
    SOAP = 2,
    REST = 3
}
typedef list<RPCProtocol> RPCProtocolArray

struct Version {
    1: required i16 versionIdentifier;
    2: required i16 featureSetIdentifier;
    3: required RPCProtocolArray rpcProtocolArray;
    4: required string manufacturerVersion;
}

struct Address {
    1: optional string aliasAddress;
    2: optional binary ieeeAddress
    3: optional binary networkAddress
}

struct TxOptions {
    1: required bool acknowledged;
    2: required bool permitFragmentation;
    3: required bool securityEnabled;
    4: required bool useNetworkKey;
}

struct ZCLCommand {
    1: optional byte destinationAddressMode;
    2: optional Address destinationAddress;
    3: optional byte destinationEndpoint;
    4: optional binary profileId;               // 2 byte
    5: required binary clusterId;               // 2 byte
    6: optional byte sourceEndpoint;
    7: required TxOptions txOptions;
    8: optional i16 radius;
    9: required binary zclHeader;
    10: required binary zclPayload;
}

struct ZCLMessage {
    1: required byte apsStatus;
    2: optional i64 rxTime;
    3: optional byte destinationEndpoint;
    4: optional byte sourceAddressMode;
    5: optional Address sourceAddress;
    6: optional byte sourceEndpoint;
    7: optional binary profileId;
    8: optional binary clusterId;
    9: optional binary zclHeader;
    10: optional binary zclPayload;
}
