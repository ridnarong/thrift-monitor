namespace java th.or.nectec.zbgateway

struct RemoteCommandReponse {
	1:required byte recievedCommand
	2:required byte status    
}

struct LocalCommandRequest {
	1:required byte command
	2:optional binary payload
}

struct LocalCommandResponse {
	1:required byte command
	2:optional binary payload
}

struct Address {
	1:optional string aliasAddress
	2:optional binary ieeeAddress
	3:optional binary networkAddress
}
struct RemoteCommandRequest {
	1:required byte destinationAddressMode
	2:required Address destinationAddress
	3:required byte transactionSeqNo
	4:required byte command
	5:optional binary payload
}

struct IndicationCommandRequest {
	1:required byte sourceAddressMode
	2:required Address sourceAddress
	3:required byte linkQuality
	4:required byte transactionSeqNo
	5:required byte command
	6:optional binary payload
}

struct ReportCommandRequest {
	1:required binary pan_id
	2:required binary sourceNetworkAddress
	3:required binary sourceIEEEAddress
	4:required binary deviceId
	5:required binary data
}

service ZigbeeGatewayService {
	LocalCommandResponse localCommand(1:LocalCommandRequest command);
	RemoteCommandReponse remoteCommand(1:RemoteCommandRequest command);
}

service IndicationService {
	oneway void indicationCommand(1:IndicationCommandRequest command);
}

service ReportService {
	oneway void sendReport(1:ReportCommandRequest command)
}
