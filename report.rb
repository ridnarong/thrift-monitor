require 'thrift'
require_relative 'transport/rb/amqp_transport'
require 'net/http'

require_relative 'gen-rb/report_service'
require_relative 'transport/rb/multiplexed_protocol'

begin
  transport = Thrift::AmqpTransport.new({heartbeat: 900}, {
    publish_exchange_type: :topic,
    publish_exchange_name: "services",
    publish_routing_key: "report_service",
  })
  protocol = Thrift::MultiplexedProtocol.new(Thrift::BinaryProtocol.new(transport),"ReportService")
  client = ReportService::Client.new(protocol)
  
  transport.open
  req = ReportCommandRequest.new
  req.pan_id = [0,0,0,0].pack("c*")
  req.sourceNetworkAddress = [192,168,0,103].pack("c*")
  req.sourceIEEEAddress = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].pack("c*")
  req.deviceId = [25190154].pack("L>")
  data = []
  str = Net::HTTP.get('192.168.0.103', '/data.htm')
  str.scan(/(\d+\.\d+)/) { |f| data.push(f[0].to_f)}
  req.data = data.pack("g*")
  print data
  client.sendReport(req)
  transport.close
rescue Thrift::Exception => tx
  print 'Thrift::Exception: ', tx.message, "\n"
end
