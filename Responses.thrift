include "Types.thrift"

namespace java com.nectec.est.zigbee_gateway.zgd.services.thrift.zgd.response

struct GetVersionResponse {
    1: required byte status;
    2: required Types.Version version;
}

struct GetAttrResponse  {
    1: required byte status;
    2: optional binary value;
}

struct SetAttrResponse  {
    1: required byte status;
}

struct SendZCLCommandResponse {
    1: required byte status;
    2: optional binary requestIdentifier;
    3: optional i64 callbackIdentifier;
    4: required Types.ZCLMessage message;
}

struct RemoteCommandResponse {
	1:required byte recievedCommand
	2:required byte status
}

struct LocalCommandResponse {
	1:required byte command
	2:optional binary payload
}