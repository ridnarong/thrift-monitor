$:.push('gen-rb')
$:.push('transport/rb')
$:.unshift '../../lib/rb/lib'

require 'bunny'
require 'securerandom'
require 'json'
require 'mongo'
require 'bson'
require 'thrift'
require 'amqp_transport'

require 'zigbee_gateway_service'
require 'multiplexed_protocol'

$target = {setpoint: nil, temp: nil}
dbclient = Mongo::Client.new(ENV['MONGODB_URI'])

transport = Thrift::AmqpTransport.new({},{ 
subscribe_exchange_type: :direct,
subscribe_exchange_name: "responses" ,
publish_exchange_type: :topic,
publish_exchange_name: "services" ,
publish_routing_key: "zigbee_gateway.0",
publish_message_reply_to: "",
 })
protocol = Thrift::MultiplexedProtocol.new(Thrift::BinaryProtocol.new(transport),"ZigbeeGatewayService")
client = ZigbeeGatewayService::Client.new(protocol)
transport.open()
amqp_conn = Bunny.new({ host: ENV['AMQP_HOST'], port: ENV['AMQP_PORT'], user: ENV['AMQP_USER'], password: ENV['AMQP_PASS'], vhost: "sensors"})
amqp_conn.start
ch = amqp_conn.create_channel
q = ch.queue("", :exclusive => true)
x = ch.topic("amq.topic")
q.bind(x, routing_key: "sensors.air.02-519-04")
q.subscribe(:block => true) do |delivery_info, properties, payload|
  payload = JSON.parse(payload)
  if payload["sensor"] == "TempSetpoint"
    $target[:setpoint] = payload["value"] 
  elsif payload["sensor"] == "InputTemp"
    $target[:temp] = payload["value"]
  end
  puts $target
  if $target[:setpoint] and ( $target[:setpoint] >= ($target[:temp] - 0.2)  and $target[:setpoint] <= ($target[:temp] + 0.2) )
    req = RemoteCommandRequest.new
    req.destinationAddressMode = 3
    addr = Address.new
    addr.ieeeAddress = ["AC10020207010300"].pack("H16")
    req.destinationAddress = addr
    req.transactionSeqNo = SecureRandom.random_number(255)
    req.command = 166
    req.payload = [($target[:setpoint] + 2)*100].pack("S>")
    t = Time.now
    dbclient[:tasks].insert_one({
          device_id: BSON::ObjectId.from_string("52e1c0ccb35111f566413be7"),
          correlation_id:  req.transactionSeqNo,
          name: "Setpoint-Test",
          command_type: "remote_command",
          message: {"destinationAddressMode" => 3, "destinationAddress" => {"ieeeAddress" => ["AC10020207010300"]},
		 "command" => 166, "payload" => req.payload.unpack("S>")}, 
          response: nil,
          created_at: t,
          updated_at: t})
    res = client.remoteCommand(req)
    if res.class == RemoteCommandReponse
      output = {from: "ZigbeeGateway", at: Time.now.getlocal("+07:00"), body: {recievedCommand: res.recievedCommand, status: res.status}}
      $target[:setpoint] = $target[:setpoint] - 2
      puts output
    end
  end
  
end
