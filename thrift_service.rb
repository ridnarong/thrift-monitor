$:.push('gen-rb')
$:.push('transport/rb')
$:.push('server/rb')
$:.unshift '../../lib/rb/lib'

require 'json'
require 'bunny'
require 'thrift'
require 'amqp_server_transport'
require 'amqp_server'

require 'indication_service'
require 'report_service'
require './command_parser'



class IndicationServiceHandler

  def initialize()
    amqp_conn = Bunny.new({ host: ENV['AMQP_HOST'], port: ENV['AMQP_PORT'], user: ENV['AMQP_USER'], password: ENV['AMQP_PASS'], vhost: ENV['AMQP_VHOST']})
    amqp_conn.start
    ch = amqp_conn.create_channel
    @x = ch.topic("amq.topic")
  end

  def indicationCommand(command)
    c,p = CommandParser.decode([command.command].pack('C').unpack('C')[0], command.payload)
  	res = {from: "indication_service", at: Time.now.getlocal("+07:00").strftime("%FT%T.%L%:z"),
  			body: {sourceAddressMode: command.sourceAddressMode,
  				sourceAddress: { aliasAddress: command.sourceAddress.aliasAddress,
  					ieeeAddress: command.sourceAddress.ieeeAddress ? command.sourceAddress.ieeeAddress.unpack('H16'):nil,
  					networkAddress: command.sourceAddress.networkAddress ? command.sourceAddress.networkAddress.unpack('H4'):nil },
  				 command: c, payload: p, linkQuality: [command.linkQuality].pack('C').unpack('C')[0],
  				 transactionSeqNo: [command.transactionSeqNo].pack('C').unpack('C')[0] }}
    @x.publish(res, :routing_key => "indication_service", :content_type => "application/json")
  end
end

class ReportServiceHandler
  def initialize()
    amqp_conn = Bunny.new({ host: ENV['AMQP_HOST'], port: ENV['AMQP_PORT'], user: ENV['AMQP_USER'], password: ENV['AMQP_PASS'], vhost: ENV['AMQP_VHOST']})
    amqp_conn.start
    ch = amqp_conn.create_channel
    @x = ch.topic("amq.topic")
  end

  def sendReport(command)
    deviceId = command.deviceId.unpack('L>')[0].to_s
    report = {data: [], id: deviceId }
    now = Time.now.to_i
    puts deviceId
    case deviceId[-2..-1]
    when "00" #Climate Sensor
      report[:data] = [ { at: now, tags: { pan_id: command.pan_id.unpack('H4')[0],
        sourceNetworkAddress: command.sourceNetworkAddress.unpack('H4')[0],
        sourceIEEEAddress: command.sourceIEEEAddress.unpack('H16')[0],
        deviceId: deviceId, type: deviceId[-2..-1], possition: deviceId[-4..-3],
        room_code: deviceId[-7..-5], building_code: deviceId[-1*deviceId.length..-8]
        }, values: { "Temperature" => command.data.unpack('S>S>')[0]/100.0,
        "RelativeHumidity" => command.data.unpack('S>S>')[1]/100.0 } }]
    when "01" #Coolant Sensor
      report[:data] = [ { at: now, tags: { pan_id: command.pan_id.unpack('H4')[0],
        sourceNetworkAddress: command.sourceNetworkAddress.unpack('H4')[0],
        sourceIEEEAddress: command.sourceIEEEAddress.unpack('H16')[0],
        deviceId: deviceId, type: deviceId[-2..-1], possition: deviceId[-4..-3],
        room_code: deviceId[-7..-5], building_code: deviceId[-1*deviceId.length..-8]
        }, values: { "Temperature" => command.data.unpack('S>')[0]/100.0 } }]   
    when "02" #Climate Controller
      report[:data] = [ { at: now, tags: { pan_id: command.pan_id.unpack('H4')[0],
        sourceNetworkAddress: command.sourceNetworkAddress.unpack('H4')[0],
        sourceIEEEAddress: command.sourceIEEEAddress.unpack('H16')[0],
        deviceId: deviceId, type: deviceId[-2..-1], possition: deviceId[-4..-3],
        room_code: deviceId[-7..-5], building_code: deviceId[-1*deviceId.length..-8]
        }, values: { "Mode" => command.data.unpack('CS>S>S>S>CC')[0],
        "InputTemp" => command.data.unpack('CS>S>S>S>CC')[1]/100.0,
        "InputRH" => command.data.unpack('CS>S>S>S>CC')[2]/100.0,
        "HeatIndex" => command.data.unpack('CS>S>S>S>CC')[3]/100.0,
        "TempSetpoint" => command.data.unpack('CS>S>S>S>CC')[4]/100.0,
        "ValvePosition" => command.data.unpack('CS>S>S>S>CC')[5],
        "FanSpeed" => command.data.unpack('CS>S>S>S>CC')[6]} }]
    when "04" #Power meter
      report[:data] = [ { at: now, tags: { pan_id: command.pan_id.unpack('H4')[0],
        sourceNetworkAddress: command.sourceNetworkAddress.unpack('H4')[0],
        sourceIEEEAddress: command.sourceIEEEAddress.unpack('H16')[0],
        deviceId: deviceId, type: deviceId[-2..-1], possition: deviceId[-4..-3],
        room_code: deviceId[-7..-5], building_code: deviceId[-1*deviceId.length..-8]
        }, values: { "FirstPhaseVoltage" => command.data.unpack('L>*')[0]/1000.0,
        "FirstPhaseAmpere" => command.data.unpack('L>*')[1]/1000.0,
        "FirstPhasePower" => command.data.unpack('L>*')[2]/1000.0,
        "FirstPhaseEnergy" => command.data.unpack('L>*')[3]/1000.0,
        "SecondPhaseVoltage" => command.data.unpack('L>*')[4]/1000.0,
        "SecondPhaseAmpere" => command.data.unpack('L>*')[5]/1000.0,
        "SecondPhasePower" => command.data.unpack('L>*')[6]/1000.0,
        "SecondPhaseEnergy" => command.data.unpack('L>*')[7]/1000.0,
        "ThirdPhaseVoltage" => command.data.unpack('L>*')[8]/1000.0,
        "ThirdPhaseAmpere" => command.data.unpack('L>*')[9]/1000.0,
        "ThirdPhasePower" => command.data.unpack('L>*')[10]/1000.0,
        "ThirdPhaseEnergy" => command.data.unpack('L>*')[11]/1000.0,
        "TotalEnergy" => command.data.unpack('L>*')[12]/1000.0
        }}]
    end

    @x.publish(report.to_json, :routing_key => "report", :content_type => "application/json")
  end
end

indication_handler = IndicationServiceHandler.new
report_handler = ReportServiceHandler.new
processor = Thrift::MultiplexedProcessor.new
processor.register_processor("IndicationService",IndicationService::Processor.new(indication_handler))
processor.register_processor("ReportService",ReportService::Processor.new(report_handler))
transport = Thrift::AmqpServerTransport.new({},{exchange_name: "services", subscribe_routing_key: ["indication_service", "report_service"]})
server = Thrift::AmqpServer.new(processor, transport)

puts "Starting server..."
server.serve
puts "OK"
