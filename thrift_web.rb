$:.push('gen-rb')
$:.push('transport/rb')
$:.unshift '../../lib/rb/lib'

require 'sinatra/base'
require 'pubnub'
require 'json'
require 'mongo'
require 'thrift'
require 'amqp_transport'

require 'zigbee_gateway_service'
require 'multiplexed_protocol'

require './command_parser'

class ThriftWeb < Sinatra::Base
  configure do
#   dbclient = Mongo::Client.new(ENV['MONGODB_URI'], { auth_mech: :scram, database: "airest_development", auth_source: "airest_development", connect: :direct})
#   set :mongo_client, dbclient
   transport = Thrift::AmqpTransport.new({heartbeat: 900},{ 
     subscribe_exchange_type: :direct,
     subscribe_exchange_name: "responses" ,
     publish_exchange_type: :topic,
     publish_exchange_name: "services" ,
     publish_routing_key: "zigbee_gateway.0",
     publish_message_reply_to: "",
   })
   protocol = Thrift::MultiplexedProtocol.new(Thrift::BinaryProtocol.new(transport),"ZigbeeGatewayService")
   client = ZigbeeGatewayService::Client.new(protocol)
   transport.open()
   set :thrift_client, client
   pubnub = Pubnub.new(subscribe_key: ENV['PUBNUB_SUB_KEY'], publish_key: ENV['PUBNUB_PUB_KEY'])
   set :pubnub, pubnub
  end

	get '/' do
		File.read(File.join('public', 'index.html'))
	end

	get '/hi' do
          settings.pubnub.publish(channel: "thrift", message: {from: "thrift-monitor", at: Time.now.getlocal("+07:00"), body: "hello world"}) do |envelope| 
            puts envelope.parsed_response
          end
		"hello world"
	end

	post '/zigbee_gateway' do
		data = JSON.parse request.body.read.to_s
    output = ""
    puts data
		if data && ["local_command", "remote_command"].include?(data["command_type"]) 
      begin
				if data["command_type"] == "local_command"
					if data.has_key?("command") && data["command"].class == Fixnum
						req = LocalCommandRequest.new
						req.command = data["command"]
						if data["message"] && data["message"].has_key?("payload") && data["message"]["payload"].class == Array
							req.payload = CommandParser.encode(data["command"],data["message"]["payload"])
						end
        settings.pubnub.publish(channel: "thrift", message:{from: "thrift-monitor", at: Time.now.getlocal("+07:00"), body: data["message"]} ) do |envelope| 
            puts envelope.parsed_response
          end

						t = Time.now.getlocal("+07:00")
            res = settings.thrift_client.localCommand(req)
						if res.class == LocalCommandResponse
              c,p = CommandParser.decode([res.command].pack('C').unpack('C')[0], res.payload)
              output = {from: "ZigbeeGateway", at: Time.now.getlocal("+07:00"), body: {command: c, payload: p}}
						end
					end
				else data["command_type"] == "remote_command"
					if data["message"] && (data["message"].has_key?("destinationAddressMode") && data["message"]["destinationAddressMode"].class == Fixnum) && (data["message"].has_key?("destinationAddress")) && (data.has_key?("command") && data["command"].class == Fixnum)
						req = RemoteCommandRequest.new
						req.destinationAddressMode = data["message"]["destinationAddressMode"]
						addr = Address.new
						if data["message"]["destinationAddress"]["aliasAddress"]
							addr.aliasAddress = data["message"]["destinationAddress"]["aliasAddress"]
						end
						if data["message"]["destinationAddress"]["ieeeAddress"]
							addr.ieeeAddress = data["message"]["destinationAddress"]["ieeeAddress"].pack('H16')
						end
						if data["message"]["destinationAddress"]["networkAddress"]
							addr.networkAddress = data["message"]["destinationAddress"]["networkAddress"].pack('H4')
						end
            unless data["message"]["transactionSeqNo"]
              data["message"]["transactionSeqNo"] = SecureRandom.random_number(254)
            end
						req.destinationAddress = addr
						req.transactionSeqNo = data["message"]["transactionSeqNo"]
						req.command = data["command"]
            t = Time.now.getlocal("+07:00")
						if data["message"].has_key?("payload") && data["message"]["payload"].class == Array
							req.payload = CommandParser.encode(data["command"], data["message"]["payload"])
						end
settings.pubnub.publish(channel: "thrift", message:{from: "thrift-monitor", at: Time.now.getlocal("+07:00"), body: data["message"]} ) do |envelope| 
            puts envelope.parsed_response
          end
						res = settings.thrift_client.remoteCommand(req)
						if res.class == RemoteCommandReponse
              output = {from: "ZigbeeGateway", at: Time.now.getlocal("+07:00"), body: {recievedCommand: res.recievedCommand, status: res.status}}
						end
					end		
				end
			rescue Exception => tx
				output = {from: "thrift-monitor", at: Time.now.getlocal("+07:00"), body: tx.class.to_s+": "+ tx.message}
			end
		end
          settings.pubnub.publish(channel: "thrift", message: output) do |envelope| 
            puts envelope.parsed_response
          end
		output.to_json
	end
  # start the server if ruby file executed directly
  run! if app_file == $0
end
