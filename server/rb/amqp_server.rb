module Thrift
  class AmqpConsumer < Bunny::Consumer 
    def cancelled?
      @cancelled
    end

    def handle_cancellation(_)
      @cancelled = true
    end
  end
  class AmqpServer < BaseServer
    def serve
      begin
        @server_transport.listen
        subscribe_channel, subscribe_queue = @server_transport.accept
        consumer = AmqpConsumer.new(subscribe_channel, subscribe_queue)
        consumer.on_delivery() do |delivery_info, properties, payload|
          response = Bytes.empty_byte_buffer
          transport = IOStreamTransport.new StringIO.new(payload), response
          protocol = @protocol_factory.get_protocol transport
          @processor.process protocol, protocol
          @server_transport.reply response, properties[:reply_to], properties[:correlation_id]
        end
        subscribe_queue.subscribe_with(consumer, :block => true)
      ensure
        @server_transport.close
      end
    end
  end
end
