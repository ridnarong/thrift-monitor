#!/usr/bin/env ruby

require 'thrift'
require_relative 'transport/rb/amqp_transport'
require 'net/http'

require_relative 'gen-rb/report_service'
require_relative 'transport/rb/multiplexed_protocol'

begin
  transport = Thrift::AmqpTransport.new({heartbeat: 900}, {
    publish_exchange_type: :topic,
    publish_exchange_name: "services",
    publish_routing_key: "report_service",
  })
  protocol = Thrift::MultiplexedProtocol.new(Thrift::BinaryProtocol.new(transport),"ReportService")
  client = ReportService::Client.new(protocol)
  
  loop do

  transport.open
  req = ReportCommandRequest.new
  req.pan_id = [0,0,0,0].pack("c*")
  req.sourceNetworkAddress = [192,168,0,103].pack("c*")
  req.sourceIEEEAddress = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].pack("c*")
  #req.deviceId = [25190190].pack("L>")
  req.deviceId = [ENV['DEVICE_ID'].to_i].pack("L>")
  data = []
  for i in (1..4)
    data.push rand
  end
  req.data = data.pack("g*")
  print data
  client.sendReport(req)
  transport.close
  sleep ENV['DELAY'].to_i
  end
rescue Thrift::Exception => tx
  print 'Thrift::Exception: ', tx.message, "\n"
end
