require 'securerandom'
require 'bunny'

module Thrift
	class AmqpTransport < BaseTransport
		def initialize(connection_options={}, message_options={})
			@outbuf = Bytes.empty_byte_buffer
			@connection_options = { 
        host: connection_options[:host] || ENV['AMQP_HOST'] || "localhost",
        port: connection_options[:port] || ENV['AMQP_PORT'] || 5672 ,
        ssl: connection_options[:ssl] || ENV['AMQP_SSL'] || false ,
        vhost: connection_options[:vhost] || ENV['AMQP_VHOST'] || "/" ,
        user: connection_options[:user] || ENV['AMQP_USER'] || "guest" ,
        pass: connection_options[:pass] || ENV['AMQP_PASS'] || "guest" ,
        heartbeat: connection_options[:heartbeat] || ENV['AMQP_HEARTBEAT'] || :server ,
        frame_max: connection_options[:frame_max] || ENV['AMQP_FRAME_MAX'] || 131072,
        auth_mechanism: connection_options[:auth_mechanism] || ENV['AMQP_AUTH_MECHANISM'] || "PLAIN" ,
        recover_from_connection_close: connection_options[:recover_from_connection_close]
      }

      @message_options = { 
        exchange_type: message_options[:exchange_type] || :topic,
        exchange_name: message_options[:exchange_name] || "amq.topic",
        subscribe_exchange_type: message_options[:subscribe_exchange_type] || message_options[:exchange_type] || :topic,
        subscribe_exchange_name: message_options[:subscribe_exchange_name] || message_options[:exchange_name] || "amq.topic",
        publish_exchange_type: message_options[:publish_exchange_type] || message_options[:exchange_type] || :topic,
        publish_exchange_name: message_options[:publish_exchange_name] || message_options[:exchange_name] || "amq.topic",
        publish_routing_key: message_options[:publish_routing_key],
        publish_message_correlation_id: message_options[:publish_message_correlation_id] || SecureRandom.uuid,
        publish_message_expiration: message_options[:publish_message_expiration] || 60,
        publish_message_reply_to: message_options[:publish_message_reply_to],
        subscribe_routing_key: message_options[:subscribe_routing_key],
        subscribe_queue_name: message_options[:publish_message_reply_to] || message_options[:subscribe_queue_name] || ""
      }
		end

		def open
			@connection = Bunny.new @connection_options
			begin
				@connection.start
			rescue StandardError => e
				raise TransportException.new(TransportException::NOT_OPEN, "#{e.class}: #{@connection_options}")
			end
		end

		def open?
			!@connection.nil? and @connection.open?
		end

		def close
			@connection.close unless closed?
		end

		def closed?
			@connection.nil? or @connection.closed?
		end

		def read(size)
			@inbuf.read size
		end

		def write(buffer)
			@outbuf << Bytes.force_binary_encoding(buffer) 
		end

		def flush
			if open?
				begin
					subscribe_channel = @connection.create_channel
					subscribe_exchange = Bunny::Exchange.new(subscribe_channel, @message_options[:subscribe_exchange_type], @message_options[:subscribe_exchange_name])
					subscribe_queue = subscribe_channel.queue(@message_options[:subscribe_queue_name], 
							:exclusive => true, 
							:auto_delete => true)
					if @message_options[:subscribe_routing_key]
						@message_options[:subscribe_routing_key].each do |routing_key|
	            subscribe_queue.bind(subscribe_exchange, 
	              :routing_key => routing_key)
	          end
          else
          	subscribe_queue.bind(subscribe_exchange, 
              :routing_key => subscribe_queue.name)
          end
					publish_channel = @connection.create_channel
					publish_exchange = Bunny::Exchange.new(publish_channel, @message_options[:publish_exchange_type], @message_options[:publish_exchange_name])
					publish_exchange.publish(@outbuf, :routing_key => @message_options[:publish_routing_key],
						correlation_id: @message_options[:publish_message_correlation_id],
						expiration: @message_options[:publish_message_expiration]*1000,
						reply_to: subscribe_queue.name,
						timestamp: Time.now.to_i)
					if @message_options[:publish_message_reply_to]
            Timeout.timeout(@message_options[:publish_message_expiration]) do
  						subscribe_queue.subscribe(block: true) do |delivery_info, properties, payload|
  							@inbuf = StringIO.new payload
  							subscribe_channel.close
  						end
            end
					end
				@outbuf = Bytes.empty_byte_buffer
				rescue StandardError  => e
					close
					@outbuf = Bytes.empty_byte_buffer
					raise TransportException.new(TransportException::UNKNOWN, "#{e.class}: #{e.message}")
          open
				end
			end
		end
	end
end
