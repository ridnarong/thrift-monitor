require 'securerandom'
require 'bunny'

module Thrift
	class AmqpServerTransport < BaseServerTransport
		def initialize(connection_options={}, message_options={})
			@connection_options = { 
        host: connection_options[:host] || ENV['AMQP_HOST'] || "localhost",
        port: connection_options[:port] || ENV['AMQP_PORT'] || 5672 ,
        ssl: connection_options[:ssl] || ENV['AMQP_SSL'] || false ,
        vhost: connection_options[:vhost] || ENV['AMQP_VHOST'] || "/" ,
        user: connection_options[:user] || ENV['AMQP_USER'] || "guest" ,
        pass: connection_options[:pass] || ENV['AMQP_PASS'] || "guest" ,
        heartbeat: connection_options[:heartbeat] || ENV['AMQP_HEARTBEAT'] || :server ,
        frame_max: connection_options[:frame_max] || ENV['AMQP_FRAME_MAX'] || 131072,
        auth_mechanism: connection_options[:auth_mechanism] || ENV['AMQP_AUTH_MECHANISM'] || "PLAIN" ,
        recover_from_connection_close: connection_options[:recover_from_connection_close]
      }

      @message_options = { 
        exchange_type: message_options[:exchange_type] || :topic,
        exchange_name: message_options[:exchange_name] || "amq.topic",
        subscribe_exchange_type: message_options[:subscribe_exchange_type] || message_options[:exchange_type] || :topic,
        subscribe_exchange_name: message_options[:subscribe_exchange_name] || message_options[:exchange_name] || "amq.topic",
        publish_exchange_type: message_options[:publish_exchange_type] || message_options[:exchange_type] || :topic,
        publish_exchange_name: message_options[:publish_exchange_name] || message_options[:exchange_name] || "amq.topic",
        publish_routing_key: message_options[:publish_routing_key],
        publish_message_correlation_id: message_options[:publish_message_correlation_id] || SecureRandom.uuid,
        publish_message_expiration: message_options[:publish_message_expiration] || 60,
        publish_message_reply_to: message_options[:publish_message_reply_to],
        subscribe_routing_key: message_options[:subscribe_routing_key],
        subscribe_queue_name: message_options[:publish_message_reply_to] || message_options[:subscribe_queue_name] || ""
      }

		end

		attr_reader :connection

		def listen
			@connection = Bunny.new @connection_options
		end

		def accept
			unless closed?
				begin
					@connection.start
          subscribe_channel = @connection.create_channel
          subscribe_exchange = Bunny::Exchange.new(subscribe_channel, @message_options[:subscribe_exchange_type], @message_options[:subscribe_exchange_name])
          subscribe_queue = subscribe_channel.queue(@message_options[:subscribe_queue_name], 
              :exclusive => true, 
              :auto_delete => true)
          if @message_options[:subscribe_routing_key]
            @message_options[:subscribe_routing_key].each do |routing_key|
              subscribe_queue.bind(subscribe_exchange, 
                :routing_key => routing_key)
            end
          else
            subscribe_queue.bind(subscribe_exchange, 
              :routing_key => subscribe_queue.name)
          end
          return subscribe_channel, subscribe_queue
				rescue StandardError => e
					raise TransportException.new(TransportException::NOT_OPEN, "#{e.class}: #{@connection_options}")
				end
			end
		end

    def reply response, to=nil, correlation_id=nil
      if to or @message_options[:publish_message_reply_to]
        begin
          if open?
            reply_channel = @connection.create_channel
            reply_exchange = Bunny::Exchange.new(reply_channel, @message_options[:publish_exchange_type], @message_options[:publish_exchange_name])
            reply_exchange.publish(response, :routing_key => to || @message_options[:publish_message_reply_to] || @message_options[:publish_routing_key],
              correlation_id: correlation_id,
              timestamp: Time.now.to_i)
            reply_channel.close
          end 
        rescue StandardError => e
          reply_channel.close
          raise TransportException.new(TransportException::UNKNOWN, "#{e.class}: #{e.message}")
        end
      end
    end
		def open?
			!@connection.nil? and @connection.open?
		end

		def close
			@connection.close unless @connection.closed?
		end

		def closed?
			@connection.nil? or @connection.closed?
		end
	end
end
