require 'multiplexed_protocol'
module Thrift
  class MultiplexedProcessor
    SERVICE_PROCESSOR_MAP = Hash.new

    def initialize()
    end
     #**
      # 'Register' a service with this <code>MultiplexedProcessor</code>. This
      # allows us to broker requests to individual services by using the service
      # name to select them at request time.
      #
      # @param serviceName Name of a service, has to be identical to the name
      # declared in the Thrift IDL, e.g. "WeatherReport".
      # @param processor Implementation of a service, usually referred to
      # as "handlers", e.g. WeatherReportHandler implementing WeatherReport.Iface.
      #/
    def register_processor(service_name, processor)
      SERVICE_PROCESSOR_MAP[service_name] = processor
    end

    def process(iprot, oprot)
      name, type, seqid  = iprot.read_message_begin
      separator_index = name.index(Thrift::MultiplexedProtocol::SEPARATOR)
      if separator_index
        processor = SERVICE_PROCESSOR_MAP[name.split(Thrift::MultiplexedProtocol::SEPARATOR)[0]]
        if processor
          name = name.split(Thrift::MultiplexedProtocol::SEPARATOR)[1]

          if processor.respond_to?("process_#{name}")
            processor.send("process_#{name}", seqid, iprot, oprot)
            return true
          else
            puts 'Unknown function '+name
            iprot.skip(Types::STRUCT)
            iprot.read_message_end
            x = ApplicationException.new(ApplicationException::UNKNOWN_METHOD, 'Unknown function '+name)
            oprot.write_message_begin(name, MessageTypes::EXCEPTION, seqid)
            x.write(oprot)
            oprot.write_message_end
            oprot.trans.flush
            return false
          end
        end
      end

    end
  end
end
