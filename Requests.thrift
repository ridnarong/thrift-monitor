include "Types.thrift"

namespace java com.nectec.est.zigbee_gateway.zgd.services.thrift.zgd.request

struct GetAttrRequest {
    1: required string attrId;
}

struct SetAttrRequest {
    1: required string attrId;
    2: required binary value;
}

struct SendZCLCommandRequest {
    1: required i64 timeout;
    2: optional string callbackDestination;
    3: required Types.ZCLCommand command;
}

struct LocalCommandRequest {
	1:required byte command
	2:optional binary payload
}

struct RemoteCommandRequest {
	1:required byte destinationAddressMode
	2:required Types.Address destinationAddress
	3:required byte transactionSeqNo
	4:required byte command
	5:optional binary payload
}

struct IndicationCommandRequest {
	1:required byte sourceAddressMode
	2:required Types.Address sourceAddress
	3:required byte linkQuality
	4:required byte transactionSeqNo
	5:required byte command
	6:optional binary payload
}

struct ReportCommandRequest {
	1:required binary pan_id
	2:required binary sourceNetworkAddress
	3:required binary sourceIEEEAddress
	4:required byte buildingNum
	5:required binary roomNum
	6:required byte deviceNum
	7:required byte deviceType
	8:required binary data
}