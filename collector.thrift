struct Profile00 {
    1: required double temp;
    2: required double humid;
}

struct Profile01 {
    1: required double temp;
}

struct Profile02 {
    1: required byte mode;
    2: required double input_temp;
    3: required double set_point;
    4: required i16 valve_position;
    5: required i16 fan_speed;
}

struct ReportAttributes {
	1: optional i64 timestamp;
	2: binary pan_id;
	3: binary networkAddress;
	4: string node_name;
	5: required byte profile;
	6: optional Profile00 profile00;
	7: optional Profile01 profile01;
	8: optional Profile02 profile02;
}

service Collector {
	void report(1:ReportAttributes reportAttributes);
}
