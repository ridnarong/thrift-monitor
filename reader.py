from django.core.management.base import BaseCommand, CommandError
from api.models import Plug, Metric
import requests
import re
import json
from django.utils import timezone
import paho.mqtt.client as mqtt

def processor(html):
    m = re.findall(r'Plug.(\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)', html)
    return m


class Command(BaseCommand):

    def handle(self, *args, **options):
        plugs = Plug.objects.all()
        client = mqtt.Client()
        client.username_pw_set('mqtt', password='mqtt')
        client.connect("erefermon.southeastasia.cloudapp.azure.com", 1883, 60)
        for plug in plugs:
            try:
                r = requests.get('http://' + plug.ip, timeout=15)
                for p in processor(r.text):
                    Metric.objects.create(timestamp=timezone.now(), plug=plug, outlet=int(p[0]),  ActE=p[5], AppE=p[6], IRMS=p[1], POW=p[3], VA=p[4], VRMS=p[2])
                    client.publish("netsense", '{"at": "%s", "PlugID": %s, "Outlet": %s, "ActE": %s, "AppE": %s, "IRMS": %s, "POW": %s, "VA": %s, "VRMS": %s}' % (timezone.now().isoformat(), plug.id, p[0], p[5], p[6], p[1], p[3], p[4], p[2]))
            except requests.exceptions.Timeout:
                print("%s Timeout", (plug.ip, ))
