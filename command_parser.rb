class CommandParser
$commands = [nil, 

  {command: "U2Z_CMD_DEFAULT_STATUS", parser: "CC",
 keys: [["Received_command"],[["Status"],["U2Z_STATUS_SUCCESS","U2Z_STATUS_FRAME_TOO_LONG", "U2Z_STATUS_LRC_ERROR",
  "U2Z_STATUS_UNSUPPORTED","U2Z_STATUS_FAILURE","U2Z_STATUS_INVALID_LENGTH",
  "U2Z_STATUS_INVALID_VALUE","U2Z_STATUS_NOT_ON_NETWORK","U2Z_STATUS_NETWORK_FAILURE",
  "U2Z_STATUS_NETWORK_TIMED_OUT","U2Z_STATUS_BUSY","U2Z_STATUS_INCONSISTENT_STATE",
  "U2Z_STATUS_NO_MEM","U2Z_STATUS_NO_SUCH_ENTRY","U2Z_STATUS_DUPLICATE_ENTRY"]]]},

  {command: "U2Z_CMD_READ_DATE_TIME"}, 

  {command: "U2Z_CMD_READ_DATE_TIME_RSP", 
    parser: "CCCCCCCC", keys: [["Year"],["Month"], ["Date"],["Day_of_week"],
    ["Hour_mode"],["Hour"],["Minute"],["Second"]]},

   {command: "U2Z_CMD_WRITE_DATE_TIME", parser: "CCCCCCCC", keys: [["Year"],["Month"],
   ["Date"],["Day_of_week"],["Hour_mode"],["Hour"],["Minute"],["Second"]]}, 

   nil,

   {command: "U2Z_CMD_SOFT_RESET"}, 

   nil, 

   {command: "U2Z_CMD_READ_IEEE_ADDRESS"}, 

   {command: "U2Z_CMD_READ_IEEE_ADDRESS_RSP", parser: "H16", keys: [["IEEEAddress"]]}, 

   {command: "U2Z_CMD_WRITE_IEEE_ADDRESS", parser: "H16", keys: [["IEEEAddress"]]}, 

   nil,

   {command: "U2Z_CMD_READ_NET_INFO"},

  {command: "U2Z_CMD_READ_NET_INFO_RSP", parser: "H4H4H16H4H16H8H2H2H2", keys: [["ShortAddress"],
    ["ParentShortAddress"],["ParentIEEEAddress"],["PANID"],["ExtendedPANID"],["ChannelMask"],
    ["LogicalChannel"],["ProtocolVersion"],["StackProfile"]]},

  {command: "U2Z_CMD_READ_STARTUP_PARAMETERS"}, 

  {command: "U2Z_CMD_READ_STARTUP_PARAMETERS_RSP",
    parser: "H4H16H4H8C", keys: [["ShortAddress"],["ExtendedPANID"],["PANID"],["ChannelMask"],
    [["StartupControl"],["Silent start","Form","Rejoin","Associate"]]]},

    {command: "U2Z_CMD_WRITE_STARTUP_PARAMETERS", parser: "H4H16H4H8C",
      keys: [["ShortAddress"],["ExtendedPANID"],["PANID"],["ChannelMask"], [["StartupControl"],
        ["Silent start","Form","Rejoin","Associate"]]]},
  
    nil,

    {command: "U2Z_CMD_LEAVE_NETWORK", parser: "CCC", keys: [["StartupMode"],["Delay"],["Jitter"]]},
  
    nil,

    {command: "U2Z_CMD_ZIGBEE_DATA_REQ", parser: "H*", keys: [["DstAddrMode-DstAddress-TransactionSeqNo-Command-Payload"]]},

    {command: "U2Z_CMD_ZIGBEE_DATA_INDICATION", parser: "H*",
      keys: [["SrcAddrMode-SrcAddress-LinkQuality-TransactionSeqNo-Command-Payload"]]},

    {command: "U2Z_CMD_READ_LOCATION_DESCRIPTION"},

    {command: "U2Z_CMD_READ_LOCATION_DESCRIPTION_RSP", parser: "H*", keys: [["LocationDesc"]]},

    {command: "U2Z_CMD_WRITE_LOCATION_DESCRIPTION", parser: "H*", keys: [["LocationDesc"]]},

    nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,
    nil,nil,nil,nil,nil,nil,nil,

    {command: "U2Z_EXT_CMD_READ_MAX_REPORT_INTERVALS"},

    {command: "U2Z_EXT_CMD_READ_MAX_REPORT_INTERVALS_RSP", parser: "C", keys: [["MaxRepIntervals"]]},

    {command: "U2Z_EXT_CMD_WRITE_MAX_REPORT_INTERVALS", parser: "C", keys: [["MaxRepIntervals"]]},

    nil,nil,nil,nil,nil,
    
    {command: "U2Z_EXT_CMD_READ_REPORT_DESTINATION"},

    {command: "U2Z_EXT_CMD_READ_REPORT_DESTINATION_RSP", parser: "H*", keys: [["ReportDst"]]},

    {command: "U2Z_EXT_CMD_WRITE_REPORT_DESTINATION", parser: "CH16", keys: [["Index"],["ReportDst"]]},

    nil,

    {command: "U2Z_EXT_CMD_READ_TIME_SERVER_ADDRESS"},

    {command: "U2Z_EXT_CMD_READ_TIME_SERVER_ADDRESS_RSP", parser: "H16", keys: [["TimeServerAddr"]]},

    {command: "U2Z_EXT_CMD_WRITE_TIME_SERVER_ADDRESS", parser: "H16", keys: [["TimeServerAddr"]]},

    nil,

    {command: "U2Z_EXT_CMD_READ_DEVICE_ID"},

    {command: "U2Z_EXT_CMD_READ_DEVICE_ID_RSP", parser: "H8", keys: [["DeviceID"]]},

    {command: "U2Z_EXT_CMD_WRITE_DEVICE_ID", parser: "H8", keys: [["DeviceID"]]},

    nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,
    nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,

    {command: "U2Z_EXT_CMD_READ_OVERRIDE_OUTPUT"},

    {command: "U2Z_EXT_CMD_READ_OVERRIDE_OUTPUT_RSP", parser: "CC", keys: [["ValvePosition"],["FanSpeed"]]},

    {command: "U2Z_EXT_CMD_WRITE_OVERRIDE_OUTPUT", parser: "CC", keys: [["ValvePosition"],["FanSpeed"]]},

    nil,

    {command: "U2Z_EXT_CMD_READ_SETPOINT"},

    {command: "U2Z_EXT_CMD_READ_SETPOINT_RSP", parser: "S>", keys: [["Setpoint"]]},

    {command: "U2Z_EXT_CMD_WRITE_SETPOINT", parser: "S>", keys: [["Setpoint"]]},

    nil,

    {command: "U2Z_EXT_CMD_READ_CONTROL_MODE"},

    {command: "U2Z_EXT_CMD_READ_CONTROL_MODE_RSP", parser: "C", keys: [[["CtrlMode"],["auto","Timed","Manual"]]]},

    {command: "U2Z_EXT_CMD_WRITE_CONTROL_MODE", parser: "C", keys: [[["CtrlMode"],["auto","Timed","Manual"]]]},

    nil,

    {command: "U2Z_EXT_CMD_READ_TIMED_MODE_CHARAC"},

    {command: "U2Z_EXT_CMD_READ_TIMED_MODE_CHARAC_RSP", parser: "C*", keys: [["TimedModeCharac"]]},

    {command: "U2Z_EXT_CMD_WRITE_TIMED_MODE_CHARAC", parser: "CC5", keys: [["Index"],["TimedModeCharac"]]},

    nil,

    {command: "U2Z_EXT_CMD_READ_CONTROL_STAT"},

    {command: "U2Z_EXT_CMD_READ_CONTROL_STAT_RSP", parser: "CCS>S>S>H*", keys: [["ValvePosition"],["FanSpeed"],["AverageTemp"],
      ["AverageRH"],["HeatIndex"],["InputRecord"]]},

    nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,

    {command: "U2Z_EXT_CMD_READ_MESSAGE_BUFFER_INFO"},

    {command: "U2Z_EXT_CMD_READ_MESSAGE_BUFFER_INFO_RSP", parser: "CCH4*", keys: [["Available"],["NumMessages"],["MessageIDs"]]},

    {command: "U2Z_EXT_CMD_READ_MESSAGE", parser: "H4", keys: [["MessageID"]]},

    {command: "U2Z_EXT_CMD_READ_MESSAGE_RSP", parser: "H4H16CCCCCS>S>CH*", keys: [["MessageID"],["Destination"],["Start_Year"],
      ["Start_Month"],["Start_Date"],["Start_Hour"],["Start_Minute"],["Duration"],["Interval"],["NumEntries"],["MessageEntry"]]},

    {command: "U2Z_EXT_CMD_WRITE_MESSAGE", parser: "H4H16CCCCCS>S>CH*", keys: [["MessageID"],["Destination"],["Start_Year"],
      ["Start_Month"],["Start_Date"],["Start_Hour"],["Start_Minute"],["Duration"],["Interval"],["NumEntries"],["MessageEntry"]]},

    nil,

    {command: "U2Z_EXT_CMD_REMOVE_MESSAGE", parser: "H4", keys: [["MessageID"]]},

    nil,

    {command: "U2Z_EXT_CMD_READ_MESSAGE_RETX_INTERVAL"},

    {command: "U2Z_EXT_CMD_READ_MESSAGE_RETX_INTERVAL_RSP", parser: "C", keys: [["Interval"]]},

    {command: "U2Z_EXT_CMD_WRITE_MESSAGE_RETX_INTERVAL", parser: "C", keys: [["Interval"]]},

    nil,nil,

    {command: "U2Z_EXT_CMD_DEVICE_REPORT", parser: "H4H4H16H8H*", keys: [["NetworkID"],["NetAddress"],["IEEEAddress"],["DeviceID"],["Data"]]}

  ]


  def self.decode(command, payload=nil)
    resp = {command: command, payload: nil}
    p = $commands[command]
    if p
      resp[:command] = p[:command]
      if payload and p[:parser]
        pl = {}
        unpacked = payload.unpack(p[:parser])
        unpacked.each_index do |i|
          if p[:keys][i].length == 1
            pl[p[:keys][i][0]] = unpacked[i]
          elsif p[:keys][i].length == 2
            pl[p[:keys][i][0][0]] = p[:keys][i][1][unpacked[i]]
          end
        end
	resp[:payload] = pl
      end
    elsif payload
      resp[:payload] = payload.unpack("H*")
    end
    return resp[:command], resp[:payload]
  end

  def self.encode(command, payload=nil)
    p = $commands[command]
    if p
      if payload and payload.class == Array
        packed = payload.pack(p[:parser])
      end
    elsif payload and payload.class == Array
      packed = payload.pack("c*")
    end
    return packed
  end
end
