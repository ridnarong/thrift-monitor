$:.push('gen-rb')
$:.push('transport/rb')
$:.unshift '../../lib/rb/lib'

require 'thrift'
require 'amqp_client'

require 'zigbee_gateway'
require 'multiplexed_protocol'

begin
	transport = Thrift::AmqpClientTransport.new(ENV['AMQP_QUEUE'], { host: ENV['AMQP_HOST'], port: ENV['AMQP_PORT'],
 user: ENV['AMQP_USER'], password: ENV['AMQP_PASSWORD'], vhost: ENV['AMQP_VHOST'], exchange: "services"})
	protocol = Thrift::MultiplexedProtocol.new(Thrift::BinaryProtocol.new(transport),"ZCL")
	client = ZigbeeGateway::Client.new(protocol)

	transport.open()


	req = LocalCommandRequest.new
	# [12].pack('c') in array to thrift binary
	req.command = [12].pack('c')
	req.payload = [12].pack('c')

	client.localCommand(req)
	transport.close()
rescue Thrift::Exception => tx
	print 'Thrift::Exception: ', tx.message, "\n"
end
