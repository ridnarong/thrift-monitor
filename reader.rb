require 'thrift'
require_relative 'transport/rb/amqp_transport'
require 'net/http'

require_relative 'gen-rb/report_service'

plugs = ['10.99.0.168','10.99.0.169','10.99.0.170','10.99.0.171','10.99.0.172','10.99.0.173','10.99.0.174','10.99.0.175']

begin
  transport = Thrift::AmqpTransport.new({heartbeat: 900}, {
    publish_exchange_type: :topic,
    publish_exchange_name: "services",
    publish_routing_key: "report_service",
  })
  protocol = Thrift::MultiplexedProtocol.new(Thrift::BinaryProtocol.new(transport),"ReportService")
  client = ReportService::Client.new(protocol)
  transport.open
  plugs.each do |plug|
    text = Net::HTTP.get(plug, '/')
    text.scan(/Plug.(\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)\s\w+=(\d+\.\d+)/).each do |p|
      req = ReportCommandRequest.new
      req.pan_id = [0,0,0,0].pack("c*")
      req.sourceNetworkAddress = plug.split('.').map { |p| p.to_i }.pack("c*")
      req.sourceIEEEAddress = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].pack("c*")
      req.deviceId = ["2#{plug.split('.')[3].to_i.to_s.rjust(5, '0')}#{p[0].to_i.to_s.rjust(2, '0')}".to_i].pack("L>")
      req.data = [p[1].to_f, p[2].to_f, p[3].to_f, p[4].to_f, p[5].to_f].pack("g*")
      client.sendReport(req)
    end
  end
  transport.close
rescue Thrift::Exception => tx
  print 'Thrift::Exception: ', tx.message, "\n"
end
