include "Requests.thrift"
include "Responses.thrift"

namespace java com.nectec.est.zigbee_gateway.zgd.services.thrift.zgd.service

service GMO {
    Responses.GetVersionResponse getVersion();

    Responses.GetAttrResponse getAttr(1:Requests.GetAttrRequest getAttr);

    Responses.SetAttrResponse setAttr(1:Requests.SetAttrRequest setAttr);
}

service ZCL {
    Responses.SendZCLCommandResponse sendZCLCommand(1:Requests.SendZCLCommandRequest sendZclCommand);
}

service ZigbeeGatewayService {
	Responses.LocalCommandResponse localCommand(1:Requests.LocalCommandRequest command);
	Responses.RemoteCommandResponse remoteCommand(1:Requests.RemoteCommandRequest command);
}

service IndicationService {
	oneway void indicationCommand(1:Requests.IndicationCommandRequest command);
}

service ReportService {
	oneway void sendReport(1:Requests.ReportCommandRequest command)
}