/***************************************************************************//**
 * @file
 * @brief Gateway node
 * @author Witsarawat Chantaweesomboon
 * @date December 2014
 ******************************************************************************/


/*******************************************************************************
 User interface
********************************************************************************

Config Mode
===========


Key functions (Off network):
--------------------------------------------------------------------------------
SW1  - Form/join network (commissioning set)
SW2  - Choose channel
SW3  -
SW4  -
LSW1 - Toggle display/keyboard mode (Config and Application)
LSW2 - Start network using default factory settings
LSW3 - Remove all bindings
LSW4 -
--------------------------------------------------------------------------------


Key functions (On network):
--------------------------------------------------------------------------------
SW1  - Toggle Permit Join (ZC/ZR only)
SW2  - Bind
SW3  -
SW4  -
LSW1 - Toggle display/keyboard mode (Config and Application)
LSW2 - Leave network
LSW3 - Remove all bindings
LSW4 -
--------------------------------------------------------------------------------


LEDs:
--------------------------------------------------------------------------------
LED1 (On)    - On network
     (Blink) - Off network
     (Off)   -
     (Blip)  -
LED2 (On)    - Joining permitted (ZC/ZR only)
     (Blink) -
     (Off)   - Joining not permitted (ZC/ZR only)
     (Blip)  -
LED3 (On)    - Bound to another device
     (Blink) - Binding / unbinding in progress
     (Off)   - Not bound to another device
     (Blip)  -
LED4 (On)    -
     (Blink) -
     (Off)   -
     (Blip)  -

LEDs running - Forming/Joining network
--------------------------------------------------------------------------------


Application Mode
================

Key functions (Off network):
--------------------------------------------------------------------------------
SW1  -
SW2  -
SW3  - Toggle Identify mode on/off
SW4  - Recall scene(must store scene  first with LSW4)
LSW1 - Toggle display/keyboard mode (Config and Application)
LSW2 -
LSW3 - Add any devices in identify mode to a group
LSW4 - Add any devices in identify mode to scene
--------------------------------------------------------------------------------


Key functions (On network):
--------------------------------------------------------------------------------
SW1  -
SW2  -
SW3  - Toggle Identify mode on/off
SW4  - Recall scene (must store scene  first with LSW4)
LSW1 - Toggle display/keyboard mode (Config and Application)
LSW2 -
LSW3 - Add any devices in identify mode to a group
LSW4 - Add any devices in identify mode to scene
--------------------------------------------------------------------------------


LEDs:
--------------------------------------------------------------------------------
LED1 (On)    -
     (Blink) - Network error
     (Off)   -
     (Blip)  - Network Tx/Rx
LED2 (On)    -
     (Blink) - UART error
     (Off)   -
     (Blip)  - UART Tx/Rx
LED3 (On)    - Added to a group
     (Blink) - Identify on
     (Off)   - Not added to group
     (Blip)  -
LED4 (On)    - Scene stored
     (Blink) -
     (Off)   - Scene not stored
     (Blip)  -
--------------------------------------------------------------------------------



********************************************************************************
Data structure of device reports
(See U2Z_EXT_CMD_DEVICE_REPORT for frame format.)
********************************************************************************

Climate Sensor
==============

Field              Length  Data Type   Description
--------------------------------------------------------------------------------
Temperature        2       Signed      Temperature in 0.01 Celsius
RelativeHumidity   2       Unsigned    Relative humidity in 0.01%


Coolant Sensor
==============

Field              Length  Data Type   Description
--------------------------------------------------------------------------------
Temperature        2       Signed      Temperature in 0.01 Celsius


Climate Controller
==================

Field              Length  Data Type   Description
--------------------------------------------------------------------------------
Mode               1       Unsigned    Operation mode
                                         0 - Auto
                                         1 - Override
                                         2 - Time-dependent
InputTemp          2       Signed      Input temperature in 0.01 Celsius
InputRH            2       Unsigned    Input relative humidity in 0.01%
HeatIndex          2       Signed      Calculated heat index in 0.01 Celsius
TempSetpoint       2       Signed      Temperature setpoint in 0.01 Celsius
ValvePosition      1       Unsigned    Coolant valve position in %
FanSpeed           1       Unsigned    Fan speed in %


Electric Meter
==============

Field              Length  Data Type   Description
--------------------------------------------------------------------------------
PhaseData1         16      Complex     Power phase data #1
PhaseData2         16      Complex     Power phase data #2
PhaseData3         16      Complex     Power phase data #3
TotalEnergy        4       Unsigned    Accumulated electric energy in Watt-hour
                                       from three power phases

PhaseData:

Field              Length  Data Type   Description
--------------------------------------------------------------------------------
Voltage            4       Unsigned    Potential difference in mV
Current            4       Unsigned    Current in mA
Power              4       Unsigned    Power in mW
Energy             4       Unsigned    Accumulated energy in W-Hr

*/


#include "BeeStack_Globals.h"
#include "BeeStackConfiguration.h"
#include "BeeStackParameters.h"
#include "AppZdoInterface.h"
#include "TS_Interface.h"
#include "TMR_Interface.h"
#include "AppAfInterface.h"
#include "FunctionLib.h"
#include "PublicConst.h"
#include "keyboard.h"
#include "Display.h"
#include "EndPointConfig.h"
#include "BeeApp.h"
#include "ZDOStateMachineHandler.h"
#include "ZdoApsInterface.h"
#include "BeeAppInit.h"
#include "NVM_Interface.h"
#include "ZtcInterface.h"
#include "HaProfile.h"
#include "ZclOptions.h"
#include "ZdpManager.h"
#include "ASL_ZdpInterface.h"
#include "ASL_UserInterface.h"
#include "ASL_ZCLInterface.h"
#include "Led.h"



#include "ApsMgmtInterface.h"

#include "DS1302.h"
#include "UARTToZigBee.h"
#include "ManuFrame.h"
#include "MemUtil.h"
#include "StringUtil.h"
#include "WatchdogTimer.h"
#include "NetUtil.h"

#include "OperatorMessage.h"

// #define APP_NO_DEBUG
#include "AppDebug.h"

/******************************************************************************
*******************************************************************************
* Private Macros
*******************************************************************************
******************************************************************************/

/* For lab test */
#define TEST_REPORTING                            FALSE

#define CLUSTER_ID                                0x8001

#define MAX_CLIMATE_TEMPERATURE                   10000
#define MIN_CLIMATE_TEMPERATURE                   0

#define MAX_COOLANT_TEMPERATURE                   4500
#define MIN_COOLANT_TEMPERATURE                   500

#define MIN_TIME_COUNTER_SYNC_INTERVAL            300 // In seconds
#define MAX_TIME_COUNTER_SYNC_INTERVAL            600 // In seconds

#define MIN_REFRESH_PERIOD                        2  // In minutes
#define MAX_REFRESH_PERIOD                        6  // In minutes

// Maximum duration (in seconds) at the display node
#define MAX_DURATION_AT_DISPLAY                   ( MAX_REFRESH_PERIOD * 180 )

#define NULL_CHAR                                 '\0'

/******************************************************************************
*******************************************************************************
* Private type definitions
*******************************************************************************
******************************************************************************/

typedef enum
{
  UI_EVT_NETWORK_TX_RX,
  UI_EVT_NETWORK_ERROR,
  UI_EVT_NETWORK_OFFLINE,
  UI_EVT_UART_TX_RX,
  UI_EVT_UART_ERROR
} UIEvent_t;

/******************************************************************************
*******************************************************************************
* Private Prototypes
*******************************************************************************
******************************************************************************/

void BeeAppTask(event_t events);
void BeeAppDataIndication(void);
void BeeAppDataConfirm(void);
#if gInterPanCommunicationEnabled_c
void BeeAppInterPanDataConfirm(void);
void BeeAppInterPanDataIndication(void);
#endif



static void initializeApplication(void);
static void networkErrorAction(void);

static zclStatus_t specificClusterServer(const MF_DataReq_t * pReq);
static zclStatus_t climateSensorReport(const MF_ClimateSensorReport_t * pReport);
static zclStatus_t coolantSensorReport(const MF_CoolantSensorReport_t * pReport);
static zclStatus_t climateControllerReport(const MF_ClimateControllerReport_t * pReport);
static zclStatus_t electricMeterReport(const MF_ElectricMeterReport_t * pReport);
static zclStatus_t handleTimeRequest(void);
// dstNwkAddr in Little Endian
static zclStatus_t sendTimeResponse(const zbNwkAddr_t dstNwkAddr,
                                    zbEndPoint_t dstEndPoint,
                                    const uint8_t * pTransactionSeqNum);

static void updateUI(UIEvent_t event);
static void secondTimerCallback(tmrTimerID_t timerID);
static void timeCounterSync(void);
static bool_t readClockAndSync(RTC_ClockCalendar_t * pDateTime);

static void messageKeeper(void);
static void messageTxRoutine(void);
static void messageTxDataConfirm(bool_t status);

static void U2ZNotify(uint8_t processedCommand, U2Z_Status_t status);
static void broadcastTime(void);

extern zbStatus_t ZCL_SendDefaultResponse(zbApsdeDataIndication_t *, zclStatus_t);


#if ( TEST_REPORTING )
static void selfReport(void);
static void selfReportTransmit(const MF_DataReq_t * pDataReq);
#endif

/******************************************************************************
*******************************************************************************
* Private Memory Declarations
*******************************************************************************
******************************************************************************/

#ifndef APP_NO_DEBUG
static const char * DEBUG_PREFIX = "[BeeApp] ";
#endif

static zbApsdeDataIndication_t * pDataIndication = NULL;
static tmrTimerID_t secondTimer = gTmrInvalidTimerID_c;

static struct
{
  zbApsConfirmId_t confirmID;
  bool_t dataConfirmFlag;
} timeResponseInfo = {0xFF, FALSE};

static struct
{
  uint32_t minutes;
  uint16_t countsToSync;
} internalCounterInfo = {0};

static struct
{
  enum
  {
    MSG_TX_IDLE,
    MSG_TX_STOP_REQUEST,
    MSG_TX_IN_PROGRESS
  } state;

  index_t entryIndex;

  struct
  {
    index_t currentIndex;
    uint8_t currentNumber;
    uint8_t lastNumber;
  } linked;

  zbApsConfirmId_t confirmID;
  bool_t dataConfirmFlag;
} messageTxInfo = {MSG_TX_IDLE, 0, {0, 0, 0}, 0xFF, FALSE};

/******************************************************************************
*******************************************************************************
* Public memory declarations
*******************************************************************************
******************************************************************************/

zbEndPoint_t appEndPoint;

/* This data set contains app layer variables to be preserved across resets */
NvDataItemDescription_t const gaNvAppDataSet[] = {
  gAPS_DATA_SET_FOR_NVM,    /* APS layer NVM data */
  {&gZclCommonAttr,         sizeof(zclCommonAttr_t)},       /* scenes, location, etc... */
  {&gAslData,               sizeof(ASL_Data_t)},            /* state of ASL */
  /* insert any user data for NVM here.... */


  U2Z_DATA_SET_FOR_NVM,


  {NULL, 0}       /* Required end-of-table marker. */
};


/******************************************************************************
*******************************************************************************
* Private Functions
*******************************************************************************
******************************************************************************/

/*****************************************************************************
* BeeAppInit
*
* Initialize the application.
****************************************************************************/
void BeeAppInit
  (
  void
  )
{
  uint8_t i;

  /* register the application endpoint(s), so we receive callbacks */
  for(i=0; i<gNum_EndPoints_c; ++i) {
    (void)AF_RegisterEndPoint(endPointList[i].pEndpointDesc);
  }

  /* where to send switch commands from */
  appEndPoint = endPointList[0].pEndpointDesc->pSimpleDesc->endPoint;

  /* start with all LEDs off */
  ASL_InitUserInterface("Gateway");



  // Initialize the watchdog timer.
  WD_Initialize();

  // Initialize UART to ZigBee module.
  U2Z_Initialize(appEndPoint);
  U2Z_SetNotificationFunction(&U2ZNotify);

  /* gaNvAppDataSet restored from NVM by U2Z_Initialize(). */

  // Register private cluster server.
  MF_RegisterSpecificClusterServer(&specificClusterServer);

  initializeApplication();

  // Check App state stored in NVM (gNvDataSet_App_ID_c)
  if (appState != mStateIdle_c) // #define appState gAslData.appState
  {
    /* Recover from reset */

    AppStartNetwork(NET_START_SILENT_JOIN);
  }

  return;
}

/*****************************************************************************
* BeeAppAppTask
*
* The application task.
*****************************************************************************/
void BeeAppTask
  (
  event_t events    /*IN: events for the application task */
  )
{
  /* received one or more data confirms */
  if(events & gAppEvtDataConfirm_c)
    BeeAppDataConfirm();

  /* received one or more data indications */
  if(events & gAppEvtDataIndication_c)
    BeeAppDataIndication();

#if gInterPanCommunicationEnabled_c
    /* received one or more data confirms */
  if(events & gInterPanAppEvtDataConfirm_c)
    BeeAppInterPanDataConfirm();

  /* received one or more data indications */
  if(events & gInterPanAppEvtDataIndication_c)
    BeeAppInterPanDataIndication();
#endif


  /* ZCL specific */
  if(events & gAppEvtAddGroup_c)
    ASL_ZclAddGroupHandler();

  if(events & gAppEvtStoreScene_c)
    ASL_ZclStoreSceneHandler();

  if(events & gAppEvtSyncReq_c)
    ASL_Nlme_Sync_req(FALSE);



  if (events & gAppEvtMessage_c)
    messageTxRoutine();

  if (events & gAppEvtTimeCounterSync_c)
    timeCounterSync();

  if (events & gAppEvtClockChange_c)
    broadcastTime();

  return;
}

/*****************************************************************************
* BeeAppHandleKeys
*
* Handles all key events for this device.
*****************************************************************************/
void BeeAppHandleKeys
  (
  key_event_t events  /*IN: Events from keyboard modul */
  )
{
(void) events;
  /* Clear LCD Display */
  LCD_ClearDisplay();

  /* no application specific keys, let ASL handle all of them */
  ASL_HandleKeys(events);
}

/*****************************************************************************
* BeeAppUpdateDevice
*
* Contains application specific
*
*
* The default keyboard handling uses a model system: a network configuration-mode
* and an application run-mode. Combined with the concepts of short and
* long-press, this gives the application a total of 16 keys on a 4 button system
* (4 buttons * 2 modes * short and long).
*
* Config-mode covers joining and leaving a network, binding and other
* non-application specific keys, and are common across all Freescale applications.
*
* Run-mode covers application specific keys.
*
*****************************************************************************/
void BeeAppUpdateDevice
  (
  zbEndPoint_t endPoint,    /* IN: endpoint update happend on */
  zclUIEvent_t event,        /* IN: state to update */
  zclAttrId_t attrId,
  zbClusterId_t clusterId,
  void *pData
  )
{
  (void) attrId;
  (void) clusterId;
  (void) pData;



  switch (event)
  {
  /* Formed/joined network */
  case gZDOToAppMgmtZCRunning_c:
  case gZDOToAppMgmtZRRunning_c:
  case gZDOToAppMgmtZEDRunning_c:
    break;

  case gZDOToAppMgmtStopped_c:
  case gLeaveNetwork_c:

    updateUI(UI_EVT_NETWORK_OFFLINE);
    break;

  default:
    break;
  }


  // Let ASL handle the event.
  ASL_UpdateDevice(endPoint,event);
  return;
}

/*****************************************************************************
  BeeAppDataIndication

  Process incoming ZigBee over-the-air messages.
*****************************************************************************/
void BeeAppDataIndication
  (
  void
  )
{
  apsdeToAfMessage_t *pMsg;
  zbStatus_t status = gZclMfgSpecific_c;

  while (MSG_Pending(&gAppDataIndicationQueue))
  {
    /* Get a message from a queue */
    pMsg = MSG_DeQueue( &gAppDataIndicationQueue );

    /* ask ZCL to handle the frame */
    pDataIndication = &(pMsg->msgData.dataIndication);
    status = ZCL_InterpretFrame(pDataIndication);

    if(status == gZclMfgSpecific_c)
    {
      uint16_t clusterID = Swap2Bytes(AppTwoBytesToUint(pDataIndication->aClusterId));

      // AppDebug_PrintInt(DEBUG_PREFIX, clusterID, "Cluster ID");

      if (U2Z_CLUSTER_ID == clusterID)
        U2Z_HandleDataIndication(pDataIndication);
      else if (CLUSTER_ID == clusterID)
      {
        updateUI(UI_EVT_NETWORK_TX_RX);
        MF_InterpretFrame(pDataIndication);
      }
      else
        ZCL_SendDefaultMfgResponse(pDataIndication);
    }

    /* Free memory allocated by data indication */
    AF_FreeDataIndicationMsg(pMsg);
    pDataIndication = NULL; // Must be set to NULL.
  } // while (MSG_Pending(...

  return;
}

/*****************************************************************************
  BeeAppDataConfirm

  Process incoming ZigBee over-the-air data confirms.
*****************************************************************************/
void BeeAppDataConfirm
  (
  void
  )
{
  apsdeToAfMessage_t *pMsg;
  zbApsdeDataConfirm_t *pConfirm;

  while(MSG_Pending(&gAppDataConfirmQueue))
  {
    /* Get a message from a queue */
    pMsg = MSG_DeQueue( &gAppDataConfirmQueue );
    pConfirm = &(pMsg->msgData.dataConfirm);

    // Give the UART to ZigBee module the first check.
    if (!U2Z_HandleDataConfirm(pConfirm))
    {
      ZclApsAckConfirm(pConfirm->dstAddr.aNwkAddr, pConfirm->status);

      if (timeResponseInfo.dataConfirmFlag &&
          (pConfirm->confirmId == timeResponseInfo.confirmID))
      {
        timeResponseInfo.dataConfirmFlag = FALSE;

        AppDebug_PrintInt(DEBUG_PREFIX, pConfirm->status, "Time Rsp Data Confirm status");

        if (pConfirm->status == gZbSuccess_c)
          updateUI(UI_EVT_NETWORK_TX_RX);
        else
          networkErrorAction();
      }
      else if (messageTxInfo.dataConfirmFlag &&
               (pConfirm->confirmId == messageTxInfo.confirmID))
      {
        messageTxInfo.dataConfirmFlag = FALSE;

        AppDebug_PrintInt(DEBUG_PREFIX, pConfirm->status, "Msg Data Confirm status");

        if (pConfirm->status == gZbSuccess_c)
        {
          updateUI(UI_EVT_NETWORK_TX_RX);
          messageTxDataConfirm(TRUE);
        }
        else
        {
          networkErrorAction();
          messageTxDataConfirm(FALSE);
        }
      }
    }

    /* Free memory allocated in Call Back function */
    MSG_Free(pMsg);
  }

  return;
}

#if gInterPanCommunicationEnabled_c

/*****************************************************************************
  BeeAppInterPanDataIndication

  Process InterPan incoming ZigBee over-the-air messages.
*****************************************************************************/
void BeeAppInterPanDataIndication(void)
{
  InterPanMessage_t *pMsg;
  zbInterPanDataIndication_t *pIndication;
  zbStatus_t status = gZclMfgSpecific_c;

  while(MSG_Pending(&gInterPanAppDataIndicationQueue))
  {
    /* Get a message from a queue */
    pMsg = MSG_DeQueue( &gInterPanAppDataIndicationQueue );

    /* ask ZCL to handle the frame */
    pIndication = &(pMsg->msgData.InterPandataIndication );

    /* Free memory allocated by data indication */
    MSG_Free(pMsg);
  }
}


/*****************************************************************************
  BeeAppDataConfirm

  Process InterPan incoming ZigBee over-the-air data confirms.
*****************************************************************************/
void BeeAppInterPanDataConfirm
(
void
)
{
  InterPanMessage_t *pMsg;
  zbInterPanDataConfirm_t *pConfirm;

  while(MSG_Pending(&gInterPanAppDataConfirmQueue))
  {
    /* Get a message from a queue */
    pMsg = MSG_DeQueue( &gInterPanAppDataConfirmQueue );
    pConfirm = &(pMsg->msgData.InterPandataConf);

    /* Action taken when confirmation is received. */
    if( pConfirm->status != gZbSuccess_c )
    {
      /* The data wasn't delivered -- Handle error code here */
    }

    /* Free memory allocated in Call Back function */
    MSG_Free(pMsg);
  }
}
#endif

/*****************************************************************************/
/*****************************************************************************/

static void initializeApplication(void)
{
  if ((secondTimer = TMR_AllocateTimer()) != gTmrInvalidTimerID_c)
    TMR_StartIntervalTimer(secondTimer, 1000, &secondTimerCallback);
#ifndef APP_NO_DEBUG
  else
    AppDebug_Print(DEBUG_PREFIX, "TMR_AllocateTimer");
#endif

  return;
}

/*****************************************************************************/

static void networkErrorAction(void)
{
  if (ZDO_GetState() != gZdoInitialState_c) // ZDO not stopped
    updateUI(UI_EVT_NETWORK_ERROR);
}

/*****************************************************************************/

static zclStatus_t specificClusterServer(const MF_DataReq_t * pReq)
{
  switch (pReq->command)
  {
  case MF_ZCL_CMD_CLIMATE_SENSOR_REPORT:
    return climateSensorReport(&pReq->structuredData.climateSensorReport);

  case MF_ZCL_CMD_COOLANT_SENSOR_REPORT:
    return coolantSensorReport(&pReq->structuredData.coolantSensorReport);

  case MF_ZCL_CMD_CLIMATE_CONTROLLER_REPORT:
    return climateControllerReport(&pReq->structuredData.climateControllerReport);

  case MF_ZCL_CMD_TIME_REQUEST:
    return handleTimeRequest();

  case MF_ZCL_CMD_ELECTRIC_METER_REPORT:
    return electricMeterReport(&pReq->structuredData.electricMeterReport);

  default:
    AppDebug_PrintInt(DEBUG_PREFIX, pReq->command, "Unsupported command");
    return gZclUnsupportedMfgClusterCommand_c;
  }
}

/*****************************************************************************/

static zclStatus_t climateSensorReport(const MF_ClimateSensorReport_t * pReport)
{
  zbIeeeAddr_t IEEEAddr;
  uint8_t data[4];

  AppDebug_PrintInt(DEBUG_PREFIX, pReport->sensorID, "Climate Sensor report");

  if (APS_GetIeeeAddress(pDataIndication->aSrcAddr, IEEEAddr) == NULL)
  {
    /* Set to FF's if the IEEE address is not known. */
    AppSetMem(IEEEAddr, sizeof(zbIeeeAddr_t), 0xFF);
  }

  /* Temperature */

  if (pReport->temperature > MAX_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(data, MAX_CLIMATE_TEMPERATURE);
  else if (pReport->temperature < MIN_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(data, MIN_CLIMATE_TEMPERATURE);
  else
    AppSetTwoBytes(data, pReport->temperature);

  /* Relative humidity */

  if (pReport->relativeHumidity > 10000)
    AppSetTwoBytes(&data[2], 10000);
  else
    AppSetTwoBytes(&data[2], pReport->relativeHumidity);

  if (!U2Z_SendDeviceReportCommand(pDataIndication->aSrcAddr,
                                   IEEEAddr,
                                   pReport->sensorID,
                                   data,
                                   sizeof(data)))
  {
    AppDebug_Print(DEBUG_PREFIX, "UART command failure");
    return gZclFailure_c;
  }

  return gZclSuccessDefaultRsp_c;
}

/*****************************************************************************/

static zclStatus_t coolantSensorReport(const MF_CoolantSensorReport_t * pReport)
{
  zbIeeeAddr_t IEEEAddr;
  uint8_t data[2];

  AppDebug_PrintInt(DEBUG_PREFIX, pReport->sensorID, "Coolant Sensor report");

  if (APS_GetIeeeAddress(pDataIndication->aSrcAddr, IEEEAddr) == NULL)
  {
    /* Set to FF's if the IEEE address is not known. */
    AppSetMem(IEEEAddr, sizeof(zbIeeeAddr_t), 0xFF);
  }

  /* Temperature */

  if (pReport->temperature > MAX_COOLANT_TEMPERATURE)
    AppSetTwoBytes(data, MAX_COOLANT_TEMPERATURE);
  else if (pReport->temperature < MIN_COOLANT_TEMPERATURE)
    AppSetTwoBytes(data, MIN_COOLANT_TEMPERATURE);
  else
    AppSetTwoBytes(data, pReport->temperature);

  if (!U2Z_SendDeviceReportCommand(pDataIndication->aSrcAddr,
                                   IEEEAddr,
                                   pReport->sensorID,
                                   data,
                                   sizeof(data)))
  {
    AppDebug_Print(DEBUG_PREFIX, "UART command failure");
    return gZclFailure_c;
  }

  return gZclSuccessDefaultRsp_c;
}

/*****************************************************************************/

static zclStatus_t climateControllerReport(const MF_ClimateControllerReport_t * pReport)
{
  zbIeeeAddr_t IEEEAddr;
  uint8_t data[11];

  AppDebug_PrintInt(DEBUG_PREFIX, pReport->controllerID, "Controller report");

  if (APS_GetIeeeAddress(pDataIndication->aSrcAddr, IEEEAddr) == NULL)
  {
    /* Set to FF's if the IEEE address is not known. */
    AppSetMem(IEEEAddr, sizeof(zbIeeeAddr_t), 0xFF);
  }

  /* Mode */

  data[0] = pReport->mode;

  /* InputTemp */

  if (pReport->inputTemp > MAX_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(&data[1], MAX_CLIMATE_TEMPERATURE);
  else if (pReport->inputTemp < MIN_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(&data[1], MIN_CLIMATE_TEMPERATURE);
  else
    AppSetTwoBytes(&data[1], pReport->inputTemp);

  /* InputRH */

  if (pReport->inputRH > 10000)
    AppSetTwoBytes(&data[3], 10000);
  else
    AppSetTwoBytes(&data[3], pReport->inputRH);

  /* HeatIndex */

  if (pReport->heatIndex > MAX_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(&data[5], MAX_CLIMATE_TEMPERATURE);
  else if (pReport->heatIndex < MIN_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(&data[5], MIN_CLIMATE_TEMPERATURE);
  else
    AppSetTwoBytes(&data[5], pReport->heatIndex);

  /* TempSetpoint */

  if (pReport->tempSetpoint > MAX_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(&data[7], MAX_CLIMATE_TEMPERATURE);
  else if (pReport->tempSetpoint < MIN_CLIMATE_TEMPERATURE)
    AppSetTwoBytes(&data[7], MIN_CLIMATE_TEMPERATURE);
  else
    AppSetTwoBytes(&data[7], pReport->tempSetpoint);

  /* ValvePosition */

  if (pReport->valvePosition > 100)
    data[9] = 100;
  else
    data[9] = pReport->valvePosition;

  /* FanSpeed */

  if (pReport->fanSpeed > 100)
    data[10] = 100;
  else
    data[10] = pReport->fanSpeed;

  if (!U2Z_SendDeviceReportCommand(pDataIndication->aSrcAddr,
                                   IEEEAddr,
                                   pReport->controllerID,
                                   data,
                                   sizeof(data)))
  {
    AppDebug_Print(DEBUG_PREFIX, "UART command failure");
    return gZclFailure_c;
  }

  return gZclSuccessDefaultRsp_c;
}

/*****************************************************************************/

static zclStatus_t electricMeterReport(const MF_ElectricMeterReport_t * pReport)
{
  zbIeeeAddr_t IEEEAddr;
  uint8_t data[52], * ptr = data;
  index_t i;

  AppDebug_PrintInt(DEBUG_PREFIX, pReport->meterID, "Electric Meter report");

  if (APS_GetIeeeAddress(pDataIndication->aSrcAddr, IEEEAddr) == NULL)
  {
    /* Set to FF's if the IEEE address is not known. */
    AppSetMem(IEEEAddr, sizeof(zbIeeeAddr_t), 0xFF);
  }

  for (i = 0; i < 3; ++i)
  {
    /* Voltage */

    AppSetFourBytes(ptr, pReport->phase[i].voltage);
    ptr += 4;

    /* Current */

    AppSetFourBytes(ptr, pReport->phase[i].current);
    ptr += 4;

    /* Power */

    AppSetFourBytes(ptr, pReport->phase[i].power);
    ptr += 4;

    /* Energy */

    AppSetFourBytes(ptr, pReport->phase[i].energy);
    ptr += 4;
  }

  /* TotalEnergy */

  AppSetFourBytes(ptr, pReport->totalEnergy);
  // ptr += 4;

  if (!U2Z_SendDeviceReportCommand(pDataIndication->aSrcAddr,
                                   IEEEAddr,
                                   pReport->meterID,
                                   data,
                                   sizeof(data)))
  {
    AppDebug_Print(DEBUG_PREFIX, "UART command failure");
    return gZclFailure_c;
  }

  return gZclSuccessDefaultRsp_c;
}

/*****************************************************************************/

static zclStatus_t handleTimeRequest(void)
{
  const zclMfgFrame_t * pMftFrame = (zclMfgFrame_t *) pDataIndication->pAsdu;

  AppDebug_Print(DEBUG_PREFIX, "Time request");

  return sendTimeResponse(pDataIndication->aSrcAddr,
                          pDataIndication->srcEndPoint,
                          &pMftFrame->transactionId);
}

/*****************************************************************************/

static zclStatus_t sendTimeResponse(const zbNwkAddr_t dstNwkAddr,
                                    zbEndPoint_t dstEndPoint,
                                    const uint8_t * pTransactionSeqNum)
{
  RTC_ClockCalendar_t clock;
  afAddrInfo_t addrInfo;
  MF_DataReq_t req;

  if (!readClockAndSync(&clock))
    return gZclFailure_c;

  BeeUtilZeroMemory(&addrInfo, sizeof(afAddrInfo_t));

  addrInfo.dstAddrMode = gZbAddrMode16Bit_c;
  AppCopyMem(addrInfo.dstAddr.aNwkAddr, dstNwkAddr, sizeof(zbNwkAddr_t));
  addrInfo.dstEndPoint = dstEndPoint;
  AppSetTwoBytes(addrInfo.aClusterId, Swap2Bytes(CLUSTER_ID));
  addrInfo.srcEndPoint = appEndPoint;
  addrInfo.txOptions = gApsTxOptionAckTx_c;
  addrInfo.radiusCounter = 0; // Radius not specified

  req.command = MF_ZCL_CMD_TIME_RESPONSE;
  req.structuredData.timeResponse.year = clock.year;
  req.structuredData.timeResponse.month = clock.month;
  req.structuredData.timeResponse.date = clock.date;
  req.structuredData.timeResponse.dayOfWeek = clock.dayOfWeek;
  req.structuredData.timeResponse.hour = clock.hour.value;
  req.structuredData.timeResponse.minute = clock.minute;
  req.structuredData.timeResponse.second = clock.second;

  timeResponseInfo.confirmID = MF_GetConfirmID();
  if (!MF_Transmit(&addrInfo, &req, pTransactionSeqNum, &timeResponseInfo.confirmID))
  {
    networkErrorAction();
    return gZclFailure_c;
  }

  timeResponseInfo.dataConfirmFlag = TRUE;
  return gZclSuccess_c;
}

/*****************************************************************************/

static void updateUI(UIEvent_t event)
{
  switch (event)
  {
  case UI_EVT_NETWORK_TX_RX:
    ASL_SetAppModeLED(LED1, gLedOff_c);
    ASL_SetAppModeLED(LED1, gLedBlip_c);
    break;

  case UI_EVT_NETWORK_ERROR:
    ASL_SetAppModeLED(LED1, gLedFlashing_c);
    break;

  case UI_EVT_NETWORK_OFFLINE:
    ASL_SetAppModeLED(LED1, gLedOff_c);
    break;

  case UI_EVT_UART_TX_RX:
    ASL_SetAppModeLED(LED2, gLedOff_c);
    ASL_SetAppModeLED(LED2, gLedBlip_c);
    break;

  case UI_EVT_UART_ERROR:
    ASL_SetAppModeLED(LED2, gLedFlashing_c);
    break;

  default:
    AppDebug_PrintInt(DEBUG_PREFIX, event, "Illegal UI event");
    break;
  }

  return;
}

/*****************************************************************************/

static void secondTimerCallback(tmrTimerID_t timerID)
{
  static uint8_t wdCounts = 0, seconds = 0;

  if (timerID != secondTimer)
  {
    AppDebug_PrintInt(DEBUG_PREFIX, timerID, "Invalid timer");
    return;
  }

  /* ----- Watchdog ----- */

  wdCounts = (wdCounts + 1) % (WD_TIME_OUT / 1000);
  if (wdCounts == 0)
  {
    // AppDebug_Print(DEBUG_PREFIX, "Reset WD");
    WD_Service();
  }

  /* ----- Message routine ----- */

  // Wait until we've got time from the clock at start-up.
  if (internalCounterInfo.minutes > 0)
  {
    seconds = (seconds + 1) % 60;
    if (seconds == 0)
    {
      // Update the time counter.
      internalCounterInfo.minutes += 1;
      messageKeeper();
    }
  }

  /* ----- Time counter sync ----- */

  if (internalCounterInfo.countsToSync > 0)
    internalCounterInfo.countsToSync -= 1;
  if (internalCounterInfo.countsToSync == 0)
  {
    TS_SendEvent(gAppTaskID, gAppEvtTimeCounterSync_c);
    internalCounterInfo.countsToSync = MIN_TIME_COUNTER_SYNC_INTERVAL;
  }


#if ( TEST_REPORTING )
  selfReport();
#endif

  return;
}

/*****************************************************************************/

static void timeCounterSync(void)
{
  RTC_ClockCalendar_t dateTime;

  (void) readClockAndSync(&dateTime);
  return;
}

/*****************************************************************************/

static bool_t readClockAndSync(RTC_ClockCalendar_t * pDateTime)
{
  OpMsg_DateTime_t dt;

  if (!RTC_ReadClockCalendar(pDateTime))
  {
    AppDebug_Print(DEBUG_PREFIX, "RTC error");
    return FALSE;
  }

  if (pDateTime->hour.m12Hour)
    RTC_ConvertHourFormat(&pDateTime->hour);

  dt.year = pDateTime->year;
  dt.month = pDateTime->month;
  dt.date = pDateTime->date;
  dt.hour = pDateTime->hour.value;
  dt.minute = pDateTime->minute;

  internalCounterInfo.minutes = OpMsg_ToMinutes(dt);
  internalCounterInfo.countsToSync = MAX_TIME_COUNTER_SYNC_INTERVAL;

  AppDebug_Print(DEBUG_PREFIX, "Time counter sync'ed");

  return TRUE;
}

/*****************************************************************************/

static void messageKeeper(void)
{
  index_t iEntry;
  bool_t eventFlag = FALSE;

  /*
  if (internalCounterInfo.minutes == 0)
    return;
  */

  for (iEntry = 0; iEntry < OP_MSG_BUFFER_SIZE; ++iEntry)
  {
    OpMsg_MessageEntry_t * pEntry = &OpMsg_MessageBuffer[iEntry];

    if (pEntry->length == 0)
      continue; // Null entry

    if (pEntry->flags & OP_MSG_FLAG_LINKED_TO_PREVIOUS)
      continue; // Ignore intermediate/last linked entry

    if (pEntry->flags & OP_MSG_FLAG_TX_IN_PROGRESS)
    {
      AppDebug_PrintInt(DEBUG_PREFIX, pEntry->properties.ID, "Message tx in progress");
      continue;
    }

    if (internalCounterInfo.minutes < pEntry->startMinute)
    {
      /* Pending message */

      AppDebug_PrintInt(DEBUG_PREFIX, pEntry->properties.ID, "Inactive message");

      if (pEntry->flags & OP_MSG_FLAG_DELETE)
      {
        AppDebug_Print(DEBUG_PREFIX, "Removed");
        OpMsg_DeleteMessage(pEntry);
      }

      continue;
    }

    if (internalCounterInfo.minutes > (pEntry->startMinute + pEntry->properties.duration))
    {
      /* Expired */

      AppDebug_PrintInt(DEBUG_PREFIX, pEntry->properties.ID, "Message expired");
      OpMsg_DeleteMessage(pEntry);
      continue;
    }

    if (pEntry->countsToTransmit > 0)
      pEntry->countsToTransmit -= 1;

    if (pEntry->countsToTransmit == 0)
    {
      pEntry->flags |= OP_MSG_FLAG_TX;
      eventFlag = TRUE;
    }
  } // for (iEntry...

  if (eventFlag)
    TS_SendEvent(gAppTaskID, gAppEvtMessage_c);

  return;
}

/*****************************************************************************/

static void messageTxRoutine(void)
{
  index_t iEntry;
  OpMsg_MessageEntry_t * pEntry;

  if (internalCounterInfo.minutes == 0)
  {
    AppDebug_Print(DEBUG_PREFIX, "No valid time from RTC");
    return;
  }

  if (messageTxInfo.state != MSG_TX_IDLE)
  {
    AppDebug_Print(DEBUG_PREFIX, "Message tx in progress");
    return;
  }

  iEntry = messageTxInfo.entryIndex; // Starting index
  do
  {
    pEntry = &OpMsg_MessageBuffer[iEntry];

    if ( (pEntry->length > 0) &&
         ((pEntry->flags & OP_MSG_FLAG_LINKED_TO_PREVIOUS) == 0) &&
         ( (pEntry->flags & OP_MSG_FLAG_TX) ||
           (pEntry->flags & OP_MSG_FLAG_DELETE) ) )
    {
      break;
    }

    pEntry = NULL;

    // Increment the index.
    iEntry = (iEntry + 1) % OP_MSG_BUFFER_SIZE;
  } while (iEntry != messageTxInfo.entryIndex);

  if (pEntry == NULL)
  {
    AppDebug_Print(DEBUG_PREFIX, "No message to send");
    return;
  }

  // Remember the current entry index.
  messageTxInfo.entryIndex = iEntry;

  AppDebug_PrintInt(DEBUG_PREFIX, pEntry->properties.ID, "Sending message");

  if (internalCounterInfo.minutes < pEntry->startMinute)
  {
    /* Pending message */

    AppDebug_Print(DEBUG_PREFIX, "Inactive");

    if (pEntry->flags & OP_MSG_FLAG_DELETE)
    {
      AppDebug_Print(DEBUG_PREFIX, "Removed");
      OpMsg_DeleteMessage(pEntry);
    }
    else if (pEntry->flags & OP_MSG_FLAG_TX)
      pEntry->flags &= ~OP_MSG_FLAG_TX;
  }
  else if ((internalCounterInfo.minutes - pEntry->startMinute) >= pEntry->properties.duration)
  {
    /* Expired */

    AppDebug_Print(DEBUG_PREFIX, "Expired");
    OpMsg_DeleteMessage(pEntry);
  }
  else if (!ZDO_IsRunningState())
  {
    /* Not on network */

    AppDebug_PrintInt(DEBUG_PREFIX, ZDO_GetState(), "ZDO state");

    if (pEntry->flags & OP_MSG_FLAG_DELETE)
    {
      AppDebug_Print(DEBUG_PREFIX, "Removed");
      OpMsg_DeleteMessage(pEntry);
    }
    else if (pEntry->flags & OP_MSG_FLAG_TX)
    {
      pEntry->flags &= ~OP_MSG_FLAG_TX;
      pEntry->countsToTransmit = MIN_REFRESH_PERIOD;
    }
  }
  else
  {
    afAddrInfo_t addrInfo;
    MF_DataReq_t req;
    MF_Message_t * pMessage;

    req.command = MF_ZCL_CMD_MESSAGE;
    pMessage = &req.structuredData.message;

    BeeUtilZeroMemory(&addrInfo, sizeof(afAddrInfo_t));

    addrInfo.dstAddrMode = gZbAddrMode64Bit_c;
    AppCopyMem(addrInfo.dstAddr.aIeeeAddr,
               pEntry->properties.destination,
               sizeof(zbIeeeAddr_t));
    Swap8Bytes(addrInfo.dstAddr.aIeeeAddr); // Change to little endian
    addrInfo.dstEndPoint = appEndPoint;
    AppSetTwoBytes(addrInfo.aClusterId, Swap2Bytes(CLUSTER_ID));
    addrInfo.srcEndPoint = appEndPoint;
    addrInfo.txOptions = gApsTxOptionAckTx_c;
    addrInfo.radiusCounter = 0; // Radius not specified

    pMessage->interval = pEntry->properties.interval;
    pMessage->ID = pEntry->properties.ID;

    if (pEntry->flags & OP_MSG_FLAG_DELETE)
    {
      AppDebug_Print(DEBUG_PREFIX, "Stop request");

      pMessage->duration = 0;
      pMessage->fragmentNumber = 0; // Not used
      pMessage->lastFragmentNumber = 0; // Not used
      pMessage->length = 0; // Not used

      messageTxInfo.state = MSG_TX_STOP_REQUEST;

      // Reset variables
      messageTxInfo.linked.currentNumber = 0;
      messageTxInfo.linked.lastNumber = 0;
    } // OP_MSG_FLAG_DELETE
    else if (pEntry->flags & OP_MSG_FLAG_TX)
    {
      uint32_t timeLeft = ( pEntry->properties.duration -
                            (internalCounterInfo.minutes - pEntry->startMinute) ) *
                          60; // In seconds

      pMessage->duration = (timeLeft > MAX_DURATION_AT_DISPLAY)
                           ? MAX_DURATION_AT_DISPLAY
                           : (uint16_t) timeLeft;

      AppDebug_PrintInt(DEBUG_PREFIX, pMessage->duration, "Duration (s) at display");

      if (pEntry->flags & OP_MSG_FLAG_LINKED_TO_NEXT)
      {
        /* Linked message */

        AppDebug_Print(DEBUG_PREFIX, "Linked message");

        if (messageTxInfo.linked.lastNumber > 0)
        {
          /* Currently transmitting */

          if (messageTxInfo.linked.currentNumber >= messageTxInfo.linked.lastNumber)
          {
            /* Done */

            AppDebug_Print(DEBUG_PREFIX, "Completed");

            pEntry->flags &= ~OP_MSG_FLAG_TX;
            pEntry->countsToTransmit = MAX_REFRESH_PERIOD;
          }
          else
          {
            const OpMsg_MessageEntry_t * p = &OpMsg_MessageBuffer[messageTxInfo.linked.currentIndex];

            // Next linked message
            messageTxInfo.linked.currentIndex = p->next;
            messageTxInfo.linked.currentNumber += 1;
            p = &OpMsg_MessageBuffer[p->next];

            pMessage->fragmentNumber = messageTxInfo.linked.currentNumber;
            pMessage->lastFragmentNumber = messageTxInfo.linked.lastNumber;
            pMessage->length = p->length;
            AppCopyMem(pMessage->str, p->message, p->length);

            AppDebug_PrintInt(DEBUG_PREFIX, pMessage->fragmentNumber, "Fragment no.");
            AppDebug_PrintInt(DEBUG_PREFIX, pMessage->lastFragmentNumber, "Last Fragment no.");

            messageTxInfo.state = MSG_TX_IN_PROGRESS;
          }
        }
        else
        {
          /* Starting transmitting */

          const OpMsg_MessageEntry_t * p = pEntry;

          // Count number of linked message (starting from 0).
          while (p->flags & OP_MSG_FLAG_LINKED_TO_NEXT)
          {
            messageTxInfo.linked.lastNumber += 1;
            p = &OpMsg_MessageBuffer[p->next];
          }

          messageTxInfo.linked.currentIndex = messageTxInfo.entryIndex;
          messageTxInfo.linked.currentNumber = 0;

          pMessage->fragmentNumber = messageTxInfo.linked.currentNumber;
          pMessage->lastFragmentNumber = messageTxInfo.linked.lastNumber;
          pMessage->length = pEntry->length;
          AppCopyMem(pMessage->str, pEntry->message, pEntry->length);

          AppDebug_PrintInt(DEBUG_PREFIX, pMessage->fragmentNumber, "Fragment no.");
          AppDebug_PrintInt(DEBUG_PREFIX, pMessage->lastFragmentNumber, "Last Fragment no.");

          messageTxInfo.state = MSG_TX_IN_PROGRESS;
        }
      } // OP_MSG_FLAG_LINKED_TO_NEXT
      else
      {
        /* Single message */

        pMessage->fragmentNumber = 0;
        pMessage->lastFragmentNumber = 0;
        pMessage->length = pEntry->length;
        AppCopyMem(pMessage->str, pEntry->message, pEntry->length);

        AppDebug_Print(DEBUG_PREFIX, "Non-linked message");

        messageTxInfo.state = MSG_TX_IN_PROGRESS;
      }
    } // OP_MSG_FLAG_TX

    if (messageTxInfo.state != MSG_TX_IDLE)
    {
      /* Transmit */

      messageTxInfo.confirmID = MF_GetConfirmID();
      if (MF_Transmit(&addrInfo, &req, NULL, &messageTxInfo.confirmID))
      {
        messageTxInfo.dataConfirmFlag = TRUE;
        return; // Continue at Data Confirm
      }

      /* Transmission failed */

      AppDebug_Print(DEBUG_PREFIX, "Tx failed");

      networkErrorAction();

      if (messageTxInfo.state == MSG_TX_STOP_REQUEST)
      {
        AppDebug_Print(DEBUG_PREFIX, "Removed");
        OpMsg_DeleteMessage(pEntry);
      }
      else if (messageTxInfo.state == MSG_TX_IN_PROGRESS)
      {
        pEntry->flags &= ~OP_MSG_FLAG_TX;
        pEntry->countsToTransmit = MIN_REFRESH_PERIOD;
      }

      messageTxInfo.state = MSG_TX_IDLE;
    }
  } // else

  /* Got here if no message transmission. */

  // Reset variables
  messageTxInfo.linked.currentNumber = 0;
  messageTxInfo.linked.lastNumber = 0;

  TS_SendEvent(gAppTaskID, gAppEvtMessage_c);
  return;
}

/*****************************************************************************/

static void messageTxDataConfirm(bool_t status)
{
  OpMsg_MessageEntry_t * pEntry = &OpMsg_MessageBuffer[messageTxInfo.entryIndex];

  AppDebug_PrintInt(DEBUG_PREFIX, pEntry->properties.ID, "Message Tx Data Confirm");

  if (messageTxInfo.state == MSG_TX_STOP_REQUEST)
  {
    /* Delete the message regardless of whether the stop request has been
       delivered. */

    AppDebug_Print(DEBUG_PREFIX, "Removed");
    OpMsg_DeleteMessage(pEntry);
  }
  else if (messageTxInfo.state == MSG_TX_IN_PROGRESS)
  {
    if (status)
    {
      if ((pEntry->flags & OP_MSG_FLAG_LINKED_TO_NEXT) == 0) // Last linked message
      {
        pEntry->flags &= ~OP_MSG_FLAG_TX;
        pEntry->countsToTransmit = MAX_REFRESH_PERIOD;
      }
    }
    else
    {
      pEntry->flags &= ~OP_MSG_FLAG_TX;
      pEntry->countsToTransmit = MIN_REFRESH_PERIOD;

      // Reset variables
      messageTxInfo.linked.currentNumber = 0;
      messageTxInfo.linked.lastNumber = 0;
    }
  }

  messageTxInfo.state = MSG_TX_IDLE;

  TS_SendEvent(gAppTaskID, gAppEvtMessage_c);
  return;
}

/*****************************************************************************/

static void U2ZNotify(uint8_t processedCommand, U2Z_Status_t status)
{
  if ( (processedCommand == U2Z_CMD_WRITE_DATE_TIME) &&
       (status == U2Z_STATUS_SUCCESS) )
  {
    TS_SendEvent(gAppTaskID, gAppEvtClockChange_c);
  }

  return;
}

/*****************************************************************************/

static void broadcastTime(void)
{
  static uint8_t broadcastTimesSeqNum = 0;

  AppDebug_Print(DEBUG_PREFIX, "Clock changed. Broadcast time.");

  /* pTransactionSeqNum != NULL
     --> gZclFrameControl_DirectionRsp set.
     --> Server-to-client ZCL frame.
  */
  (void) sendTimeResponse(gaBroadcastAddress, appEndPoint, &broadcastTimesSeqNum);
  broadcastTimesSeqNum += 1;

  return;
}

/*****************************************************************************/

#if ( TEST_REPORTING )

static void selfReport(void)
{
  static uint32_t count = 0;

  MF_DataReq_t req;

  count += 1;

  if (!ZDO_IsRunningState())
    return;

  if ((count % 67) == 0)
  {
    MF_ElectricMeterReport_t * pReport = &req.structuredData.electricMeterReport;
    index_t i;

    req.command = MF_ZCL_CMD_ELECTRIC_METER_REPORT;

    pReport->meterID = 29990703;

    for (i = 0; i < 3; ++i)
    {
      pReport->phase[i].voltage = 220000 + (GetRandomRange(0, 200) * 100);
      pReport->phase[i].current = 10000 + (GetRandomRange(0, 200) * 10);
      pReport->phase[i].power = 2500000 + (GetRandomRange(0, 200) * 10000);
      pReport->phase[i].energy = 10000000;
    }

    pReport->totalEnergy = 30000000;

    AppDebug_Print(DEBUG_PREFIX, "Test report (electric meter)");
    selfReportTransmit(&req);
    return;
  }

  if ((count % 53) == 0)
  {
    MF_ClimateControllerReport_t * pReport;

    req.command = MF_ZCL_CMD_CLIMATE_CONTROLLER_REPORT;
    pReport = &req.structuredData.climateControllerReport;

    pReport->controllerID = 29990402;
    pReport->mode = 1;
    pReport->tempSetpoint = 2500;
    pReport->inputTemp = 2500 + GetRandomRange(0, 200);
    pReport->inputRH = 6000 + (10 * GetRandomRange(0, 200));
    pReport->heatIndex = 2500 + GetRandomRange(0, 200);
    pReport->valvePosition = GetRandomRange(0, 100);
    pReport->fanSpeed = 50 + GetRandomRange(0, 50);

    AppDebug_Print(DEBUG_PREFIX, "Test report (climate controller)");
    selfReportTransmit(&req);
    return;
  }

  if ((count % 37) == 0)
  {
    MF_ClimateSensorReport_t * pReport;

    req.command = MF_ZCL_CMD_CLIMATE_SENSOR_REPORT;
    pReport = &req.structuredData.climateSensorReport;

    pReport->sensorID = 29990100;
    pReport->temperature = 2400 + GetRandomRange(0, 200);
    pReport->relativeHumidity = 6000 + (10 * GetRandomRange(0, 200));

    AppDebug_Print(DEBUG_PREFIX, "Test report (climate sensor)");
    selfReportTransmit(&req);
    return;
  }

  if ((count % 23) == 0)
  {
    MF_CoolantSensorReport_t * pReport;

    req.command = MF_ZCL_CMD_COOLANT_SENSOR_REPORT;
    pReport = &req.structuredData.coolantSensorReport;

    pReport->sensorID = 29990501;
    pReport->temperature = 1000 + GetRandomRange(0, 200);

    AppDebug_Print(DEBUG_PREFIX, "Test report (coolant sensor)");
    selfReportTransmit(&req);
    return;
  }

  return;
}

/*****************************************************************************/

static void selfReportTransmit(const MF_DataReq_t * pDataReq)
{
  afAddrInfo_t addrInfo;

  BeeUtilZeroMemory(&addrInfo, sizeof(afAddrInfo_t));

  // Send to itself.
  addrInfo.dstAddrMode = gZbAddrMode16Bit_c;
  AppCopyMem(addrInfo.dstAddr.aNwkAddr,
             NlmeGetRequest(gNwkShortAddress_c),
             sizeof(zbNwkAddr_t));

  addrInfo.dstEndPoint = appEndPoint;
  AppSetTwoBytes(addrInfo.aClusterId, Swap2Bytes(CLUSTER_ID));
  addrInfo.srcEndPoint = appEndPoint;
  addrInfo.txOptions = gApsTxOptionAckTx_c;
  addrInfo.radiusCounter = 0; // Radius not specified

  (void) MF_Transmit(&addrInfo, pDataReq, NULL, NULL);
  return;
}

#endif /* TEST_REPORTING */
