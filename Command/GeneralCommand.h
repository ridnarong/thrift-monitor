/***************************************************************************//**
 * @file
 * @brief UART to ZigBee software module
 * @details
 * UART to ZigBee software module enables the ZigBee device to receive commands
 * from the UART port. Device's properties and network parameters can be
 * configured through UART commands.
 * @author Witsarawat Chantaweesomboon
 * @date November 2014
 ******************************************************************************/

/*

UART command frame format
~~~~~~~~~~~~~~~~~~~~~~~~~

Field          Length (bytes)
-----------------------------
SyncByte       1
CommandID      1
Length         1
Payload        <Length>
LRC            0/1

*/

#ifndef UART_TO_ZIGBEE_H_
#define UART_TO_ZIGBEE_H_ 1

#include "EmbeddedTypes.h"
#include "Zigbee.h"
#include "Zcl.h"

#include "UARTToZigBeeOptions.h"
#include "UARTToZigBeeExt.h"

/*******************************************************************************
********************************************************************************
  Public macros
********************************************************************************
*******************************************************************************/


/*******************************************************************************
 * Add the macro U2Z_DATA_SET_FOR_NVM into gaNvAppDataSet[] in BeeApp.c.
 ******************************************************************************/
#if ( U2Z_EXTENSION_INCLUDED )
  #define U2Z_DATA_SET_FOR_NVM                                             \
    {&gCommisioningServerAttrsData, sizeof(zbCommissioningAttributes_t)},  \
    {&U2Z_NVMData,                  sizeof(U2Z_NVMData_t)},                \
    {&U2Z_ExtNVMData,               sizeof(U2Z_ExtNVMData_t)}
#else
  #define U2Z_DATA_SET_FOR_NVM                                             \
    {&gCommisioningServerAttrsData, sizeof(zbCommissioningAttributes_t)},  \
    {&U2Z_NVMData,                  sizeof(U2Z_NVMData_t)}
#endif



#define U2Z_CMD_SYNC_BYTE                         0xB4


/* Status codes */

#define U2Z_STATUS_SUCCESS                        0x00
#define U2Z_STATUS_FRAME_TOO_LONG                 0x01
#define U2Z_STATUS_LRC_ERROR                      0x02
#define U2Z_STATUS_UNSUPPORTED                    0x03
#define U2Z_STATUS_FAILURE                        0x04
#define U2Z_STATUS_INVALID_LENGTH                 0x05
#define U2Z_STATUS_INVALID_VALUE                  0x06
#define U2Z_STATUS_NOT_ON_NETWORK                 0x07
#define U2Z_STATUS_NETWORK_FAILURE                0x08
#define U2Z_STATUS_NETWORK_TIMED_OUT              0x09
#define U2Z_STATUS_BUSY                           0x0A
#define U2Z_STATUS_INCONSISTENT_STATE             0x0B
#define U2Z_STATUS_NO_MEM                         0x0C
#define U2Z_STATUS_NO_SUCH_ENTRY                  0x0D
#define U2Z_STATUS_DUPLICATE_ENTRY                0x0E



/* Command identifiers */


/*
Default Status Command
~~~~~~~~~~~~~~~~~~~~~~

Description:
  Report the status of the processing of the command. This command is sent if
no other response is generated for the received command.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Received_command   1                Command to which this command responds
Status             1                Status code
*/
#define U2Z_CMD_DEFAULT_STATUS                    0x01


/*
Read Date Time Command
~~~~~~~~~~~~~~~~~~~~~~

Description:
  Read the date and time from the clock.

Direction:
  Input

Payload:
  None
*/
#define U2Z_CMD_READ_DATE_TIME                    0x02


/*
Read Date Time Response Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Report the date and time as a response to Read Date Time Command.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Year               1                Year (range: 00-99)
Month              1                Month (range: 1-12)
Date               1                Date (range: 1-31)
Day_of_week        1                Day of week (range: 1-7) (user defined)
Hour_mode          1                0 = AM, 1 = PM, 2 = 24-hour
Hour               1                Hour (range: 1-12 or 0-23)
Minute             1                Minute (range: 0-59)
Second             1                Second (range: 0-59)
*/
#define U2Z_CMD_READ_DATE_TIME_RSP                0x03


/*
Write Date Time Command
~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Set the date and time.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Year               1                Year (range: 00-99)
Month              1                Month (range: 1-12)
Date               1                Date (range: 1-31)
Day_of_week        1                Day of week (range: 1-7) (user defined)
Hour_mode          1                0 = AM, 1 = PM, 2 = 24-hour
Hour               1                Hour (range: 1-12 or 0-23)
Minute             1                Minute (range: 0-59)
Second             1                Second (range: 0-59)
*/
#define U2Z_CMD_WRITE_DATE_TIME                   0x04

/*
Soft Reset Command
~~~~~~~~~~~~~~~~~~

Description:
  Reset the MCU. The command process takes about 3 seconds.

Direction:
  Input

Payload:
  None
*/
#define U2Z_CMD_SOFT_RESET                        0x06


/*
Read IEEE address Command
~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Read the device's IEEE address.

Direction:
  Input

Payload:
  None
*/
#define U2Z_CMD_READ_IEEE_ADDRESS                 0x08


/*
Read IEEE address Response Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Report the device's IEEE address as a response to Read IEEE address Command.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
IEEEAddress        8                64-bit IEEE address
*/
#define U2Z_CMD_READ_IEEE_ADDRESS_RSP             0x09


/*
Write IEEE address Command
~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Set the device's IEEE address.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
IEEEAddress        8                64-bit IEEE address
*/
#define U2Z_CMD_WRITE_IEEE_ADDRESS                0x0A


/*
Read Network Information Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Read network information.

Direction:
  Input

Payload:
  None
*/
#define U2Z_CMD_READ_NET_INFO                     0x0C


/*
Read Network Information Response Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Report the network information as a response to Read Network Information
Command.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
ShortAddress       2                16-bit network address
ParentShortAddress 2                Parent's 16-bit network address
ParentIEEEAddress  8                Parent's 64-bit IEEE address
PANID              2                PAN ID of the network of which the device is
                                    a member.
ExtendedPANID      8                Extended PAN ID of the network of which
                                    the device is a member.
ChannelMask        4                IEEE 8.2.15.4 channel mask, i.e., the set of
                                    channels the device should scan as port of
                                    the network join or formation procedures.
LogicalChannel     1                Channel of the network of which the device
                                    is a member.
ProtocolVersion    1                Protocol version
StackProfile       1                Stack profile
*/
#define U2Z_CMD_READ_NET_INFO_RSP                 0x0D


/*
Read Startup Parameters Attribute Set Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Read the Startup Parameters attribute set of Commissioning cluster.
The attributes are stored in the non-volatile memory.

Direction:
  Input

Payload:
  None
*/
#define U2Z_CMD_READ_STARTUP_PARAMETERS           0x0E


/*
Read Startup Parameters Attribute Set Response Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Report the Startup Parameters attribute set of Commissioning cluster.
The attributes are stored in the non-volatile memory.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
ShortAddress       2                16-bit network address.
                                    (0xFFFF = unspecified value)
ExtendedPANID      8                Extended PAN ID of the network of which
                                    the device is a member.
                                    (0xFFFFFFFFFFFFFFFF = unspecified value)
PANID              2                PAN ID of the network of which the device is
                                    a member.
                                    (0xFFFF = device has not joined a network)
ChannelMask        4                IEEE 8.2.15.4 channel mask, i.e., the set of
                                    channels the device should scan as port of
                                    the network join or formation procedures.
StartupControl     1                StartupControl attribute
*/
#define U2Z_CMD_READ_STARTUP_PARAMETERS_RSP       0x0F


/*
Write Startup Parameters Attribute Set Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Set the Startup Parameters attribute set of Commissioning cluster.
The attributes are stored in the non-volatile memory.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
ShortAddress       2                16-bit network address.
                                    (Range: 0x0000 - 0xFFF7)
                                    (0xFFFF = unspecified value)
ExtendedPANID      8                Extended PAN ID of the network of which
                                    the device is a member.
                                    (Range: 0x0000000000000000 - 0xFFFFFFFFFFFFFFFE)
                                    (0xFFFFFFFFFFFFFFFF = unspecified value)
PANID              2                PAN ID of the network of which the device is
                                    a member.
                                    (0xFFFF = device has not joined a network)
ChannelMask        4                IEEE 8.2.15.4 channel mask, i.e., the set of
                                    channels the device should scan as port of
                                    the network join or formation procedures.
StartupControl     1                StartupControl attribute

StartupControl attribute usage:

Value   Description   Required Attributes   Optional Attributes   Ignored Attributes
--------------------------------------------------------------------------------
0x00    Silent start  ShortAddress,         ChannelMask           -
                      ExtendedPANID,
                      PANID

0x01    Form          ExtendedPANID         PANID,                ShortAddress,
        (ZC)                                ChannelMask

0x02    Rejoin        ExtendedPANID         ShortAddress,         PANID
        (ZR or ZED)                         ChannelMask

0x03    Associate     -                     ExtendedPANID,        ShortAddress,
        (ZR or ZED)                         ChannelMask           PANID
*/
#define U2Z_CMD_WRITE_STARTUP_PARAMETERS          0x10


/*
Leave Network Command
~~~~~~~~~~~~~~~~~~~~~

Description:
  Leave and optionally restart the network. It takes about 3 seconds before the
device leaves the network.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
StartupMode        1                0x00 - Leave the network.
                                    0x01 - Leave the network and reset factory
                                           default settings.
                                    0x02 - Leave and restart the network using
                                           the current set of Startup parameters.
                                    0x03 - Leave and restart the network using
                                           the current set of stack attributes.
Delay              1                Restart delay in seconds
Jitter             1                Random jitter range to be added to the
                                    restart delay.
*/
#define U2Z_CMD_LEAVE_NETWORK                     0x12


/*
ZigBee Data Request Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Send data or command to a ZigBee device.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
DstAddrMode        1                Addressing mode for the destination address
                                      0x00 = using binding table
                                      0x01 = 16-bit group address
                                      0x02 = 16-bit network address
                                      0x03 = 64-bit extended address
DstAddress         0/2/8            Destination address
TransactionSeqNo   1                Transaction sequence number
Command            1                Command
Payload            Variable         Command payload
*/
#define U2Z_CMD_ZIGBEE_DATA_REQ                   0x14


/*
ZigBee Data Indication Command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Command to transfer data received from a ZigBee device.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
SrcAddrMode        1                Addressing mode for the source address
                                      0x02 = 16-bit network address
                                      0x03 = 64-bit extended address
SrcAddress         2/8              Source address
LinkQuality        1                Link quality indication
TransactionSeqNo   1                Transaction sequence number
Command            1                Command
Payload            Variable         Command payload
*/
#define U2Z_CMD_ZIGBEE_DATA_INDICATION            0x15


/*
Read Location Description
~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Read Basic Cluster Location Description attribute.

Direction:
  Input

Payload:
  None
*/
#define U2Z_CMD_READ_LOCATION_DESCRIPTION         0x16


/*
Read Location Description Response
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Response to the Read Location Description Response command

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
LocationDesc       0-16             Location description (16 bytes max)
*/
#define U2Z_CMD_READ_LOCATION_DESCRIPTION_RSP     0x17


/*
Write Location Description
~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Set Basic Cluster Location Description attribute.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
LocationDesc       1-16             Location description (16 bytes max)
*/
#define U2Z_CMD_WRITE_LOCATION_DESCRIPTION        0x18

/*******************************************************************************
********************************************************************************
  Public type definitions
********************************************************************************
*******************************************************************************/

typedef struct
{
  zbIeeeAddr_t IEEEAddress;
#if ( ! gZclClusterOptionals_d )
  zclStr16_t location;
#endif
} U2Z_NVMData_t;


/*
Command handler

Parameters:
  pLen (in/out) - Pointer to a variable whose value equals the length of the
                  command payload. The function shall set this variable to the
                  length of the response payload filled in the buffer.
  pBuf (in/out) - Pointer to the buffer containing the command payload.
                  The function shall fill this same buffer with the response
                  payload.
  bufSize       - Size of the buffer. This is the maximum length of the response
                  payload that the function can put into the buffer.

Return:
  Command identifier of the response command.
*/
typedef uint8_t (*U2Z_CommandHandler_f)(uint8_t * pLen, uint8_t * pBuf, uint8_t bufSize);


typedef struct
{
  uint8_t command;
  U2Z_CommandHandler_f handler;
} U2Z_CommandMapEntry_t;


/*
Notification function

When a command has been received and processed, the application can be notified
with the status of the command processing.
 */
typedef void (*U2Z_Notification_f) (uint8_t processedCommand, U2Z_Status_t status);

/*******************************************************************************
********************************************************************************
  Public memory declarations
********************************************************************************
*******************************************************************************/

extern U2Z_NVMData_t U2Z_NVMData;

/*******************************************************************************
********************************************************************************
  Public function prototypes
********************************************************************************
*******************************************************************************/

/** Initialization function */
void U2Z_Initialize(zbEndPoint_t appEndpointNum);

/** Send UART command frame. */
bool_t U2Z_SendCommandFrame(uint8_t commandID, const uint8_t * payload, uint8_t len);

/**
 * @brief Handle ZigBee data confirm that results from the Remote Request
 *        command.
 *
 * @return \c TRUE, if the data confirm has been processed.
 */
bool_t U2Z_HandleDataConfirm(const zbApsdeDataConfirm_t * pDataConfirm);

/** Handle ZigBee data indication. */
void U2Z_HandleDataIndication(const zbApsdeDataIndication_t * pDataIndication);

/**
 * @brief Set the notification function.
 * @details
 * The notification function will be called when a command has been received and
 * processed.
 */
void U2Z_SetNotificationFunction(U2Z_Notification_f pf);

#endif  /* UART_TO_ZIGBEE_H_ */
