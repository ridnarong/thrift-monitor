/***************************************************************************//**
 * @file
 * @brief UART to ZigBee extension
 * @author Witsarawat Chantaweesomboon
 * @date December 2014
 ******************************************************************************/

#ifndef UART_TO_ZIGBEE_EXT_H_
#define UART_TO_ZIGBEE_EXT_H_ 1

/*******************************************************************************
********************************************************************************
  Public macros
********************************************************************************
*******************************************************************************/

/* Default device properties */

#define U2Z_MIN_MESSAGE_INTERVAL                  25
#define U2Z_MAX_REPORT_DATA_LENGTH                52
#define U2Z_DEFAULT_MESSAGE_RETX_INTERVAL         10



/* Command extension */


/*
Read Message Buffer Info
~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Read the information of the message buffer.

Direction:
  Input

Payload:
  None
*/
#define U2Z_EXT_CMD_READ_MESSAGE_BUFFER_INFO                0xC0


/*
Read Message Buffer Info Response
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Response to the Read Message Buffer Info command

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Available          1                Number of available buffer slots
NumMessages        1                Number of messages (occupied slots) in the
                                    buffer
MessageIDs         NumMessages x 2  Message identifiers
*/
#define U2Z_EXT_CMD_READ_MESSAGE_BUFFER_INFO_RSP            0xC1


/*
Read Message
~~~~~~~~~~~~

Description:
  Read the message stored in the buffer by the WRITE command.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
MessageID          2                Message identifier
*/
#define U2Z_EXT_CMD_READ_MESSAGE                            0xC2


/*
Read Messages Response
~~~~~~~~~~~~~~~~~~~~~~

Description:
  Response to the Read Messages command.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
MessageID          2                Message identifier
Destination        8                IEEE address of the display where this
                                    message is shown
Start_Year         1                Start date and time: year (00-99)
Start_Month        1                Start date and time: month (1-12)
Start_Date         1                Start date and time: date (1-31)
Start_Hour         1                Start date and time: hour (0-23)
Start_Minute       1                Start date and time: minute (0-59)
Duration           2                Length of time in MINUTES from the start
                                    date and time, during which the message can
                                    be shown on the display
Interval           2                Time in SECONDS before the message is
                                    re-displayed. Zero means The message is only
                                    displayed once.
NumEntries         1                Number of message entries. Linked message
                                    has multiple message entries.
MessageEntry       Variable         Message entries (x NumEntries)

MessageEntry:
Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Length             1                Length of the message
Message            Variable         Message to be printing on the display
*/
#define U2Z_EXT_CMD_READ_MESSAGE_RSP                        0xC3


/*
Write Message
~~~~~~~~~~~~~

Description:
  Write the message into the buffer and request the display node to show the
message. The message will be removed from the buffer automatically when it
expires.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
MessageID          2                Message identifier
Destination        8                IEEE address of the display where this
                                    message is shown
Start_Year         1                Start date and time: year (00-99)
Start_Month        1                Start date and time: month (1-12)
Start_Date         1                Start date and time: date (1-31)
Start_Hour         1                Start date and time: hour (0-23)
Start_Minute       1                Start date and time: minute (0-59)
Duration           2                Length of time in MINUTES from the start
                                    date and time, during which the message can
                                    be shown on the display
Interval           2                Time in SECONDS before the message is
                                    re-displayed. Zero means The message is only
                                    displayed once.
NumEntries         1                Number of message entries. Linked message
                                    has multiple message entries.
MessageEntry       Variable         Message entries (x NumEntries)

MessageEntry:
Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Length             1                Length of the message
Message            Variable         Message to be printing on the display
*/
#define U2Z_EXT_CMD_WRITE_MESSAGE                           0xC4


/*
Remove Message
~~~~~~~~~~~~~~

Description:
  Request the remote display node to stop displaying the message and remove the
message from the local buffer.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
MessageID          2                Message identifier
*/
#define U2Z_EXT_CMD_REMOVE_MESSAGE                          0xC6


/*
Read Message Retransmission Interval
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Read the time interval for the message retransmission.

Direction:
  Input

Payload:
  None
*/
#define U2Z_EXT_CMD_READ_MESSAGE_RETX_INTERVAL              0xC8


/*
Read Message Retransmission Interval Response
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Response to the Read Message Retransmission Interval command.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Interval           1                Retransmission interval in MINUTES.
                                    Zero means no retransmission.
*/
#define U2Z_EXT_CMD_READ_MESSAGE_RETX_INTERVAL_RSP          0xC9


/*
Write Message Retransmission Interval
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Description:
  Set the interval for the message retransmission.

Direction:
  Input

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
Interval           1                Retransmission interval in MINUTES.
                                    Zero means no retransmission.
*/
#define U2Z_EXT_CMD_WRITE_MESSAGE_RETX_INTERVAL             0xCA


/*
Device Report
~~~~~~~~~~~~~

Description:
  Provide device report from ZigBee network.

Direction:
  Output

Payload:

Field              Length (bytes)   Description
--------------------------------------------------------------------------------
NetworkID          2                PAN identifier
NetAddress         2                Network address
IEEEAddress        8                IEEE address (set to FFFFFFFFFFFFFFFF if not known)
DeviceID           4                Device identifier
Data               Variable         Device-specific data report
*/
#define U2Z_EXT_CMD_DEVICE_REPORT                           0xCD



#define saveNVMDataSet()                NvSaveOnIdle(gNvDataSet_App_ID_c)

/*******************************************************************************
********************************************************************************
  Public type definitions
********************************************************************************
*******************************************************************************/

#if ( U2Z_EXTENSION_INCLUDED )

typedef struct
{

  /* Declare the additional NVM data here. */

  uint8_t messageRetxInterval;

} U2Z_ExtNVMData_t;

#endif

/* Status code */
typedef uint8_t U2Z_Status_t;

/*******************************************************************************
********************************************************************************
  Public memory declarations
********************************************************************************
*******************************************************************************/

#if ( U2Z_EXTENSION_INCLUDED )

extern U2Z_ExtNVMData_t U2Z_ExtNVMData;

#endif

/*******************************************************************************
********************************************************************************
  Public function prototypes
********************************************************************************
*******************************************************************************/

/*
 * Utility function
 *
 * This function fills the buffer with the of the Default Status command and
 * returns U2Z_CMD_DEFAULT_STATUS.
 */
extern uint8_t defaultStatusCommand(uint8_t originalCmd,
                                    U2Z_Status_t status,
                                    uint8_t * pLen,
                                    uint8_t * pBuf);


/* Send device report command over the UART. */
bool_t U2Z_SendDeviceReportCommand(const zbNwkAddr_t netAddr,
                                   const zbIeeeAddr_t IEEEAddr,
                                   uint32_t deviceID,
                                   const uint8_t * pData,
                                   uint8_t length);

#endif  /* UART_TO_ZIGBEE_EXT_H_ */
